# pep9
Rust implementation of [Pep9](https://github.com/StanWarford/pep9), from Stan
Warford's "Computer Systems" (Fifth edition).

![The Web IDE has good error messages](demo.png)

This is a toy project for me. I did the initial virtual machine implementation
for homework in my computer systems course in the fall of 2020, and built the
assembler as a fun side project to assemble & execute Pep9 code without using
the provided tool.

In fall of 2022 I took a compilers course and TA'd the computer systems course,
and became interested in the project again. I reimplemented the assembler with
better error messages in mind.

I also wanted to mess around with a web alternative for Warford's Pep9 tool
(implemented in C++ with Qt) so that students wouldn't have to download the
original tool. This is implemented with CodeJar, Prism.js, and xterm.js. I also
rewrote the emulator to support the Pep9 operating system and be IO agnostic.
Since the assembler and emulator are written in Rust, they compile to
WebAssembly and can interop with JavaScript without too much difficulty.

The web tool can be found at https://mggmuggins.gitlab.io/pep9.

# Installing
Just install from source. Use [rustup](rustup.rs) to install a rust toolchain
appropriate for your operating system and follow these steps to install the
`p9asm` and `pep9` binaries in your user's cargo install directories.

```sh
git clone https://gitlab.com/MggMuggins/pep9
cd pep9
cargo install --path .
```

I've only tried to compile/run on Linux, so milage may vary (especially when
trying to run the tests). The tools themselves *should* be cross-platform.
