
obj = open("pep9_chums/tests/os.pepo", "r")
bin = open("pep9_cpu/src/os.pepb", "wb")

data = obj.read()

print(f"read data: {data}")

for byte_s in data.split():
    if byte_s == "zz":
        break
    else:
        byte = int(byte_s, 16).to_bytes(1, byteorder="big", signed=False)
        print(f"byte: {byte_s}, parsed: {byte}")
        bin.write(byte)

