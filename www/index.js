import {CodeJar} from "codejar";
import {withLineNumbers} from "codejar/linenumbers";

import Cookies from "js-cookie";

import Prism from "prismjs";
import 'prismjs/themes/prism.css';

import {Terminal} from "xterm";
import "xterm/css/xterm.css";
import {FitAddon} from "xterm-addon-fit";
import {Readline} from "xterm-readline";

import * as pep9 from "pep9";
import "./language_pep9";
import * as msg from "./messages";
import "./style.scss";

// Get better messages when panicking
pep9.set_panic_hook();

let jar = CodeJar(
    document.querySelector(".editor"),
    withLineNumbers(
        Prism.highlightElement,
        { color: "slategray" },
    ),
    {
        // Match nothing
        indentOn: /(?!)/,
        moveToNewLine: /(?!)/,
        addClosing: false,
    }
);

{
    let text;
    if (text = Cookies.get("retainText")) {
        text = atob(text);
        // Allow a user to get the welcome code back
        if (text.trim() === "") {
            text = msg.WELCOME_CODE;
        }
    } else {
        text = msg.WELCOME_CODE;
    }
    jar.updateCode(text);
}

jar.onUpdate(text => {
    Cookies.set("retainText", btoa(text), { SameSite: "Lax" });
});

let readlineAddon = new Readline();
let fitAddon = new FitAddon();

let term = new Terminal({
    fontFamily: "Consolas, Monaco, 'Andale Mono', 'Lucida Console', monospace",
    cursorStyle: "bar",
    cursorBlink: true,
});

term.loadAddon(readlineAddon);
term.loadAddon(fitAddon);

term.open(document.querySelector(".terminal"));

fitAddon.fit();
term.write(msg.WELCOME_MSG);

addEventListener("resize", (_) => {
    fitAddon.fit();
});

addEventListener("keydown", event => {
    //TODO: Term captures this event so there's no point checking it here
    /*if (event.ctrlKey && event.code === "ArrowUp") {
        jar.focus();
    }*/
    if (event.ctrlKey && event.code === "ArrowDown") {
        term.focus();
    }
});

runCli();

async function runCli() {
    let shell = new pep9.Pep9Shell();

    while (true) {
        let cmd = await readlineAddon.read(msg.PROMPT);
        await runCmd(cmd, shell);
    }
}

async function runCmd(cmd, shell) {
    let has_newline = false;

    function check_newline(bytes) {
        //console.log("write_bytes(`" + bytes + "`)");
        let bytes_str = new TextDecoder().decode(bytes);

        if (bytes_str == "") {
            // Retain previous state
        } else if (bytes_str.slice(-1) === "\n") {
            has_newline = true;
        } else {
            has_newline = false;
        }
        return bytes_str;
    }

    function write_bytes(bytes) {
        readlineAddon.write(check_newline(bytes));
    }

    let editor_content = jar.toString();

    let rslt = shell.cmd(cmd, editor_content);

    switch (rslt.kind) {
    case pep9.RsltKind.None:
        has_newline = true;
        break;
    case pep9.RsltKind.Txt:
        write_bytes(rslt.txt);
        break;
    case pep9.RsltKind.FetchInput:
        let txt = await readlineAddon.read("");
        shell.input(txt + "\n");
        has_newline = true;
        break;
    case pep9.RsltKind.RunCpu:
        let out = check_newline(shell.run());

        while (!shell.is_finished()) {
            let txt = await readlineAddon.read(out);
            shell.input(txt + "\n");
            out = check_newline(shell.run());
        }
        readlineAddon.write(out);
        break;
    default:
        console.error("shell.cmd() rslt was nil:" + rslt);
        write_bytes("JS err, see console\n");
    }

    if (!has_newline) {
        console.log("Added newline to output");
        readlineAddon.write("\u001b[31m⮐\u001b[0m\n");
    }
}

