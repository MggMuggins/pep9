const CopyWebpackPlugin = require("copy-webpack-plugin");
const path = require('path');

module.exports = {
  entry: "./bootstrap.js",
  output: {
    path: path.resolve(__dirname, "dist"),
    filename: "bootstrap.js",
  },
  plugins: [
    new CopyWebpackPlugin({
        patterns: ['index.html'],
    })
  ],
  module: {
    rules: [
        { test: /\.s?css$/i, use: [
            "style-loader",
            "css-loader",
            "sass-loader",
        ] },
    ],
  },
  experiments: {
    syncWebAssembly: true,
  },
};
