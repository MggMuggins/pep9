Prism.languages.pep9 = {
    'comment': { 'pattern': /;.*\n/, 'greedy': true },
    'string': { 'pattern': /"([^\\"]|\\.)*"/, 'greedy': true },

    'mnemonic': [
        {
            'pattern': /(stop|rettr|ret|movspa|movaflg|movflga)(?=\s)/i,
            'alias': 'keyword',
        },
        {
            'pattern': /((not|neg)|((as|ro)(r|l)))(a|x)(?=\s)/i,
            'alias': 'keyword',
        },
        {
            'pattern': /(br(le|lt|eq|ne|ge|gt|v|c)?|call)(?=\s)/i,
            'alias': 'keyword',
        },
        {
            'pattern': /nop(0|1)?(?=\s)/i,
            'alias': 'keyword',
        },
        {
            'pattern': /(dec(i|o)|hexo|stro)(?=\s)/i,
            'alias': 'keyword',
        },
        {
            'pattern': /(add|sub)sp(?=\s)/i,
            'alias': 'keyword',
        },
        {
            'pattern': /(add|sub|and|or)(a|x)(?=\s)/i,
            'alias': 'keyword',
        },
        {
            'pattern': /(cp|ld|st)(b|w)(a|x)(?=\s)/i,
            'alias': 'keyword',
        },
    ],
    "dotop": {
        'pattern': /\.(addrss|align|ascii|block|burn|byte|end|equate|word)(?=\s|$)/i,
        'alias': 'keyword',
    },
    'symbol': /\w+(?=:)/,

    'char': /'''|''|('\\?.')|('\\x[0-9a-f]{2}')/,
    // numbers are contextual (fully numeric labels are allowed), so it's
    // non-trivial to highlight them

    'builtin': /char(In|Out)(?=\s?,)/,
};

