export const WELCOME_CODE = `; Welcome to the Pep9 Web IDE!
; You can get started with some Pep9 code in the editor.
; Use the terminal below to assemble the current editor
br main
msg:	.ascii "Hello, world!\\n\\x00"

main:	stro msg,d
	stop
	.end
`;

export const WELCOME_MSG = "Welcome to Pep9 Web! Type \"help\" for help\n";

export const PROMPT = "$ ";

