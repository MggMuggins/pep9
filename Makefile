# This file is to help with dev workflow for web features

all: pkg

.PHONY: pkg test

# For some reason the dev profile is not the default
# Also all the web stuff is behind a non-default feature flag so make sure
# to enable that.
pkg:
	wasm-pack build --dev -- --features web

test:
	cargo test --features web
