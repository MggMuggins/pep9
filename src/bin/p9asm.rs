use std::env;
use std::fs;
use std::path::PathBuf;
use std::process;

use clap::Parser;

#[derive(Parser)]
#[command(
    name = clap::crate_name!(),
    author = clap::crate_authors!(", "),
    version = clap::crate_version!(),
    about = clap::crate_description!(),
)]
struct Cli {
    infile: String,

    /// Filename to write to. Defaults to $(basename INFILE).pepo
    outfile: Option<String>,
}

fn main() {
    let cli = Cli::parse();

    let asm = fs::read_to_string(&cli.infile)
        .unwrap_or_else(|e| {
            eprintln!("pep9_chums: failed to open {}: {}", cli.infile, e);
            process::exit(1);
        });

    let obj = pep9::assemble_or_exit(&asm, &cli.infile);

    let outfile = cli.outfile
        .map(|outfile| PathBuf::from(&outfile) )
        .unwrap_or_else(|| {
            let mut outfile = PathBuf::from(&cli.infile);
            outfile.set_extension("pepo");
            outfile
        });

    fs::write(&outfile, obj)
        .unwrap_or_else(|e| {
            let outfile = outfile.display();
            eprintln!("pep9_chums: failed to write {}: {}", outfile, e);
            process::exit(1);
        });
}
