use std::error::Error;
use std::fs;
use std::io::{self, Read, Write};

use clap::Parser;
use pep9::cpu::{Cpu, CpuStatus};

#[derive(Parser)]
#[command(
    name = clap::crate_name!(),
    author = clap::crate_authors!(", "),
    version = clap::crate_version!(),
    about = clap::crate_description!(),
)]
struct Cli {
    file: String,

    /// When present, FILE is a .pepo object code file instead of assembly
    #[arg(short, long, default_value_t = false)]
    obj: bool,
}

fn main() -> Result<(), Box::<dyn Error>> {
    let mut stdout = io::stdout();
    let mut stdin = io::stdin();
    let mut buf = [0; 240];

    let cli = Cli::parse();

    let code = fs::read_to_string(&cli.file)
        .map_err(|e|
            format!("pep9: failed to read from {}: {:#}", cli.file, e)
        )?;

    let obj = if !cli.obj {
        pep9::assemble_or_exit(&code, &cli.file)
    } else {
        code
    };

    let mut cpu = Cpu::new();
    cpu.input(obj.as_bytes());
    cpu.load();

    let mut cpu_status = CpuStatus::Running;

    while let CpuStatus::Running = cpu_status {
        let (status, output) = cpu.run();
        stdout.write(&output)?;
        stdout.flush()?;

        cpu_status = if status == CpuStatus::BlockedInput {
            let n = stdin.read(&mut buf)?;
            cpu.input(&buf[..n]);
            CpuStatus::Running
        } else {
            status
        };
    }

    Ok(())
}
