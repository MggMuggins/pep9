#![feature(core_intrinsics)]

pub mod asm;
pub mod cpu;
mod web;

use std::process;

use ariadne::Source;

/// Convenience function for p9asm and pep9 binaries
pub fn assemble_or_exit(asm: &str, filename: &str) -> String {
    asm::asm(&asm)
        .unwrap_or_else(|errs| {
            for err in errs {
                err.report(&filename)
                    .print((filename, Source::from(&asm)))
                    .expect("pep9_chums: failed to print report")
            }
            process::exit(1);
        })
}

/// Format potentially non-unicode bytes as ascii. For non-printable characters,
/// replace with `.`.
pub(crate) fn ascii_repr(bytes: &[u8]) -> String {
    let mut rslt = String::with_capacity(bytes.len());

    for byte in bytes.iter() {
        let c = char::try_from(*byte)
            .map(|c| if c.is_ascii_control() {
                '.'
            } else {
                c
            })
            .unwrap_or('.');

        rslt.push(c);
    }
    rslt
}
