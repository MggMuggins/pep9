use crate::asm;
use super::*;

#[test]
fn breakpoints() {
    // 07 -> notx
    let prog = b"07 07 07 07 07 07 00 zz";
    // break at        ^     ^

    let mut cpu = Cpu::new();
    cpu.breakpoints = vec![2, 3, 4].into_iter().collect();
    cpu.input(prog);
    assert_eq!(cpu.load(), CpuStatus::Stopped);

    let (stat, _) = cpu.run();
    assert_eq!(stat, CpuStatus::Breakpoint(2));

    // Step exactly once. cpu.run() will execute PC before checking breakpoints
    let (stat, _) = cpu.step();
    assert_eq!(stat, CpuStatus::Running);

    let (stat, _) = cpu.run();
    assert_eq!(stat, CpuStatus::Breakpoint(4));

    let (stat, _) = cpu.run();
    assert_eq!(stat, CpuStatus::Stopped);
}

fn apply_all_u16s_unary(
    op: &str,
    expected: impl Fn(u16) -> (u16, u16),
) {
    // A compromise so this doesn't take a year. The Cpu allocates 64K for
    // memory, so let's do this once and reuse that allocation
    let mut cpu = Cpu::new();

    for reg in ["a", "x"] {
        for val in 0..=u16::MAX {
            let code = format!(
                "ldw{reg} {val},i\n{unary_op}{reg}\nstop\n.end",
                val=val,
                unary_op=op,
                reg=reg,
            );

            let obj = asm::asm(&code)
                .expect("failed to assemble");

            eprintln!("Code: `{}`", code.escape_debug());
            eprintln!("Obj:  `{}`", obj.escape_debug());

            cpu.input(obj.as_bytes());

            assert_eq!(CpuStatus::Stopped, cpu.load());

            let (status, output) = cpu.run();
            assert_eq!(CpuStatus::Stopped, status);
            assert_eq!(Vec::<u8>::new(), output);

            let (rslt, status) = expected(val);

            match reg {
                "a" => assert_eq!(rslt, cpu.acc),
                "x" => assert_eq!(rslt, cpu.indx),
                _ => unreachable!(),
            }

            assert_eq!(status, cpu.status);
        }
    }
}

#[test]
#[ignore] // Passed on Nv 21, 2022
fn not() {
    fn not(val: u16) -> (u16, u16) {
        let rslt = !val;

        let mut status = 0;
        if rslt > i16::MAX as u16 {
            status |= STAT_N;
        }
        if rslt == 0 {
            status |= STAT_Z;
        }
        (rslt, status)
    }

    apply_all_u16s_unary("not", not);
}

#[test]
#[ignore] // Passed on Nov 22, 2022
fn neg() {
    fn neg(val: u16) -> (u16, u16) {
        let (rslt, overflow) = (!val as i16).overflowing_add(1);
        let rslt = rslt as u16;
        eprintln!("neg {} -> {}", val, rslt);

        let mut status = 0;
        if rslt & 0x8000 != 0 {
            eprintln!("set N");
            status |= STAT_N;
        }
        if rslt == 0 {
           eprintln!("set Z");
           status |= STAT_Z;
        }
        if overflow {
           eprintln!("set V");
           status |= STAT_V;
        }
        (rslt, status)
    }

    apply_all_u16s_unary("neg", neg);
}

#[test]
#[ignore] // Passed on Nov 21, 2022
fn asl() {
    fn asl(val: u16) -> (u16, u16) {
        let rslt = val << 1;

        let mut status = 0;
        if rslt & 0x8000 != 0 {
            status |= STAT_N;
        }
        if rslt == 0 {
            status |= STAT_Z;
        }
        if val & 0x8000 != 0 {
            status |= STAT_C;
        } else if rslt & 0x8000 != 0 { // msb changed
            status |= STAT_V;
        }
        (rslt, status)
    }

    apply_all_u16s_unary("asl", asl);
}

#[test]
#[ignore] // Passed on Nov 21, 2022
fn asr() {
    fn asr(val: u16) -> (u16, u16) {
        let rslt = (val >> 1) | (val & 0x8000);

        let mut status = 0;
        if rslt & 0x8000 != 0 {
            status |= STAT_N;
        }
        if rslt == 0 {
            status |= STAT_Z;
        }
        if val & 0x0001 == 0x0001 {
            status |= STAT_C;
        }

        (rslt, status)
    }

    apply_all_u16s_unary("asr", asr);
}

