use std::fmt;
use std::ops::{Index, RangeInclusive};

/// This memory diverges from Warford's in that we *always* have 64KiB, his
/// only does when the OS is burned to 0xffff.
pub struct Mem {
    mem: Vec<u8>,

    rom: RangeInclusive<u16>,
}

impl Mem {
    pub fn from_os(os: &[u8], load_at: u16) -> Mem {
        let mut mem = vec![0u8; (u16::MAX as usize) + 1];

        mem[(load_at as usize)..].copy_from_slice(os);

        let rom = load_at..=((os.len() as u16) - 1 + load_at);

        Mem { mem, rom }
    }

    pub fn word(&self, msb_indx: u16) -> u16 {
        // Warford's impl also wraps around to 0x0000 if msb_indx == 0xffff
        let (lsb_indx, _) = msb_indx.overflowing_add(1);

        u16::from_be_bytes([self[msb_indx], self[lsb_indx]])
    }

    pub fn store_byte(&mut self, indx: u16, byte: u8) -> Result<(), ()> {
        if self.rom.contains(&indx) {
            Err(())
        } else {
            self.mem[indx as usize] = byte;
            Ok(())
        }
    }

    pub fn store_word(&mut self, msb_indx: u16, word: u16) -> Result<(), ()> {
        let [msb, lsb] = word.to_be_bytes();

        let (lsb_indx, _) = msb_indx.overflowing_add(1);

        // This catches index in rom
        self.store_byte(msb_indx, msb as u8)?;
        self.store_byte(lsb_indx, lsb as u8)
    }

    fn dump_fmt(
        &self,
        f: &mut fmt::Formatter,
        ignore_when: impl Fn(u16, &[u8]) -> bool,
    ) -> fmt::Result {
        write!(f, "\nAddr | {:<24}| ASCII\n", "Hex")?;
        let bytes = self.mem.chunks(8);

        let mut addr = 0usize;
        for bytes in bytes {
            if ignore_when(addr as u16, bytes) {
                addr += 8;
                continue
            }

            write!(f, "{:04x} | ", addr)?;

            for byte in bytes.iter() {
                write!(f, "{:02x} ", byte)?;
            }

            write!(f, "| ")?;

            f.write_str(&crate::ascii_repr(bytes))?;

            write!(f, "\n")?;
            addr += 8;
        }
        Ok(())
    }
}

/// We don't implement IndexMut because the OS is loaded in read-only memory,
/// aka, writes can fail
impl Index<u16> for Mem {
    type Output = u8;

    fn index(&self, index: u16) -> &u8 {
        &self.mem[index as usize]
    }
}

impl fmt::Debug for Mem {
    /// Dump self to f; ignore lines with just 0
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.dump_fmt(f, |_, bytes| bytes.iter().all(|b| *b == 0 ) )
    }
}

impl fmt::Display for Mem {
    /// Dump self to `f`; ignore lines with just 0 and lines that are entirely
    /// in ROM.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.dump_fmt(f, |addr, bytes| bytes.iter()
                .enumerate()
                .all(|(byte_indx, byte)|
                    self.rom.contains(&(addr + byte_indx as u16))
                        || *byte == 0
                )
        )
    }
}

#[cfg(test)]
pub(crate) mod test {
    use super::*;

    use crate::cpu::{DEFAULT_OS, DEFAULT_OS_LOAD_AT};

    pub fn assert_mem_is(mem: &Mem, vals: &[(u16, u8)]) {
        for loc in 0..u16::MAX {
            let mut should_be = 0;

            // Ignore the ROM
            if mem.rom.contains(&loc) {
                should_be = mem[loc];
            }

            // But allow checks there anyway
            for (set_loc, val) in vals {
                if *set_loc == loc {
                    should_be = *val;
                    break;
                }
            }

            if mem[loc] != should_be {
                dbg!(&mem.rom);
                panic!(
                    "{}0x{:04x} had 0x{:02x}, expected 0x{:02x}",
                    "incorrect mem value: ",
                    loc,
                    mem[loc],
                    should_be,
                );
            }
        }
    }

    fn default_mem() -> Mem {
        Mem::from_os(DEFAULT_OS, DEFAULT_OS_LOAD_AT)
    }

    #[test]
    fn byte() {
        let mut mem = default_mem();

        let checks = vec![
            (0xffff, DEFAULT_OS[DEFAULT_OS.len() - 1]),
        ];

        assert_mem_is(&mem, &checks);

        let checks = vec![
            (0x00c1, b'\\'),
            (0x0000, 0x12),
        ];

        for (loc, val) in checks.iter() {
            mem.store_byte(*loc, *val).unwrap();
        }

        assert_mem_is(&mem, &checks);

        assert_eq!(Err(()), mem.store_byte(0xffff, 0));
    }

    #[test]
    fn word() {
        let mut mem = default_mem();

        mem.store_word(0x0000, 0x1200).unwrap();
        mem.store_word(0x28fc, 0x7290).unwrap();

        assert_eq!(Err(()), mem.store_word(0xffff, 0));

        let checks = vec![
            (0x0000, 0x12),
            (0x28fc, 0x72),
            (0x28fd, 0x90),
        ];

        assert_mem_is(&mem, &checks);

        assert_eq!(0x1200, mem.word(0x0000));
        assert_eq!(0x7290, mem.word(0x28fc));
        assert_eq!(0x0000, mem.word(0x1823));

        // 0x5212 is byte 52 (at 0xffff in the default OS) and 12 (what we
        // stored at 0x0000 above)
        assert_eq!(0x5212, mem.word(0xffff));
    }
}

