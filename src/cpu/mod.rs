mod mem;
#[cfg(test)]
mod test;

use std::collections::HashSet;
use std::fmt;

use mem::Mem;

// I love making up new file extensions
pub const DEFAULT_OS: &[u8] = include_bytes!("os.pepb");
pub const DEFAULT_OS_LOAD_AT: u16 = 0xfc17;

const STAT_N: u16 = 0x0008;
const STAT_Z: u16 = 0x0004;
const STAT_V: u16 = 0x0002;
const STAT_C: u16 = 0x0001;

#[allow(dead_code)] // We construct using transmute
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
#[repr(u8)]
enum AddrMode {
    Immediate             = 0x00,
    Direct                = 0x01,
    Indirect              = 0x02,
    StackRelative         = 0x03,
    StackRelativeDeferred = 0x04,
    Indexed               = 0x05,
    StackIndexed          = 0x06,
    StackDeferredIndexed  = 0x07,
}

impl AddrMode {
    fn of1(instr: u8) -> AddrMode {
        match instr & 0x01 {
            0x00 => AddrMode::Immediate,
            0x01 => AddrMode::Indexed,
            _ => unreachable!(),
        }
    }

    fn of3(instr: u8) -> AddrMode {
        // Well... *cracks second beer*
        unsafe { std::mem::transmute(instr & 0x07) }
    }
}

/// See [`Cpu::run`] and [`Cpu::step`]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum CpuStatus {
    /// `stop` instruction was executed
    Stopped,
    /// Program tried to write to read-only memory
    Faulted,
    /// Program tried to read from `charIn` with an empty input buffer
    BlockedInput,

    /// PC was a breakpoint. PC is given; the instruction at `Mem[PC]` has not
    /// yet been executed.
    Breakpoint(u16),

    /// Cpu can step again
    Running,
}

/// A Pep9 virtual machine.
///
/// [`Cpu::new`] initializes with an operating system pre-loaded into RAM.
/// Use [`Cpu::with_os`] to provide a custom ROM. The OS MUST include a loader,
/// and it MUST overwrite `0xfffc` with the address of the loader.
///
/// To run a "userspace" program, use [`Cpu::input`] to load the program into
/// the input stream, and then call [`Cpu::load`] to run the loader.
///
/// The default OS's loader loads bytes as a stream of ascii hex digit pairs
/// separated by single throwaway characters. Incorrectly formatted input may
/// result in an incomplete load or unexpected CPU state.
///
/// After loading, call [`Cpu::run`] to greedily execute the program until the
/// Cpu can no longer progress. See [`CpuStatus`].
///
/// The program may also be single-stepped with [`Cpu::step`].
///
/// # Example
/// ```rust
/// # use std::io::Write;
/// # use pep9::cpu::*;
/// // A couple of nops
/// let program = b"26 26 26 00 zz";
///
/// let mut cpu = Cpu::new();
/// cpu.input(program);
/// cpu.load();
/// let (status, output) = cpu.run();
///
/// std::io::stdout().write(&output).unwrap();
/// std::io::stdout().flush().unwrap();
/// # assert_eq!(CpuStatus::Stopped, status);
/// ```
pub struct Cpu {
    //                  NZVC
    // 0b0000_0000_0000_0000
    status: u16,
    acc: u16,
    indx: u16,
    pc: u16,
    sp: u16,
    op: u8,
    opspec: u16,

    mem: Mem,

    // We need some way of buffering input when we're running in a no_std/wasm
    // context, so the CPU keeps it's own input buffer.
    // This thing holds its contents in reverse (for efficiency), so the end
    // of the vector is the first byte of input
    input: Vec<u8>,

    /// Set of memory addresses which will force `Cpu::run()` to exit with
    /// [`CpuStatus::Breakpoint`].
    pub breakpoints: HashSet<u16>,
}

impl Cpu {
    /// Initialize the Cpu with the default operating system (found in Warford
    /// chapter 8). This includes the standard ascii hex loader.
    pub fn new() -> Cpu {
        Cpu::with_os(DEFAULT_OS, DEFAULT_OS_LOAD_AT)
    }

    /// Initialize with a custom OS. Note that the OS image (`os`) MUST
    /// overwrite the system memory map vectors `0xfff4`-`0xffff`. The default
    /// vector values can be found on Warford p. 500.
    pub fn with_os(os: &[u8], load_at: u16) -> Cpu {
        let mem = Mem::from_os(os, load_at);
        Cpu {
            status: 0,
            acc: 0,
            indx: 0,
            pc: 0,
            sp: mem.word(0xfff4),
            op: 0,
            opspec: 0,
            mem,
            input: vec![],
            breakpoints: HashSet::new(),
        }
    }

    /// Add bytes to the input buffer. Note that the input buffer is equivalent
    /// to Warford's "Terminal IO" in the Pep9 simulator. To simulate "Batch
    /// IO", apply an additional whitespace byte (or ascii control, depending
    /// on the program) to the end of the input.
    pub fn input(&mut self, bytes: &[u8]) {
        let mut new_input = bytes.iter()
            .copied()
            .rev()
            .collect::<Vec<_>>();
        new_input.append(&mut self.input);
        self.input = new_input;
    }

    /// Read the input buffer. Bytes are ordered with the next byte to be read
    /// first.
    pub fn input_bytes<'a>(&'a self) -> impl Iterator<Item = u8> + 'a {
        self.input.iter().rev().copied()
    }

    /// Remove all input from the input buffer
    pub fn clear_input(&mut self) {
        self.input.clear();
    }

    /// Execute the loader (see Warford p. 472)
    ///
    /// After the loader finishes, it
    /// - clears the input, because Warford's loader does not `*grumble
    ///   grumble*`
    /// - Initializes the cpu with
    ///   - `SP <- mem[0xfff4]`
    ///   - `PC <- 0`
    pub fn load(&mut self) -> CpuStatus {
        self.sp = self.mem.word(0xfff6);
        self.pc = self.mem.word(0xfffc);

        let (status, _) = self.run();
        self.clear_input();

        self.init();

        status
    }

    /// Initialize the CPU:
    fn init(&mut self) {
        self.sp = self.mem.word(0xfff4);
        self.pc = 0;
    }

    /// Resume CPU execution from current state. See `load()` to change the
    /// program loaded at `0x0000`.
    ///
    /// This will stop executing for several reasons:
    /// - The `stop` instruction (`0x0000`) was executed
    ///   ([`CpuStatus::Stopped`]).
    /// - The Cpu encountered a fault (tried to write to ROM)
    ///   ([`CpuStatus::Faulted`]).
    /// - `self`'s input buffer was empty and the program tried to read from
    ///   `charIn` ([`CpuStatus::BlockedInput`]).
    /// - `PC` was a breakpoint ([`CpuStatus::Breakpoint`])
    ///
    /// The returned vector is the list of chars that were written to `charOut`
    pub fn run(&mut self) -> (CpuStatus, Vec<u8>) {
        let mut output = vec![];

        let status = loop {
            let (stat, out_byte) = self.step();
            if let Some(byte) = out_byte {
                output.push(byte);
            }

            if stat != CpuStatus::Running {
                break stat;
            }

            if self.breakpoints.contains(&self.pc) {
                break CpuStatus::Breakpoint(self.pc);
            }
        };
        (status, output)
    }

    /// Execute one iteration of the Von-Neuman machine. Ignores breakpoints.
    /// Note that this function will step through traps instead of over them.
    ///
    /// Returned byte is `Some` if a byte was written to `charOut`
    pub fn step(&mut self) -> (CpuStatus, Option<u8>) {
        self.op = self.mem[self.pc];
        self.pc += 1;

        if ! self.op_is_unary() {
            self.opspec = self.mem.word(self.pc);
            self.pc += 2;
        }

        self.execute()
    }

    /// Execute the instruction in self.op. Assumes opspec is already loaded.
    fn execute(&mut self) -> (CpuStatus, Option<u8>) {
        let mut out_byte = None;

        let stat = match self.op {
            // Stop
            0x00 => CpuStatus::Stopped,
            // Ret
            0x01 => {
                self.pc = self.mem.word(self.sp);
                self.sp += 2;
                CpuStatus::Running
            }
            // Rettr
            0x02 => {
                self.status = (self.mem.word(self.sp) & 0x0f00) >> 8;
                self.acc = self.mem.word(self.sp + 1);
                self.indx = self.mem.word(self.sp + 3);
                self.pc = self.mem.word(self.sp + 5);
                self.sp = self.mem.word(self.sp + 7);
                CpuStatus::Running
            }
            // MovSPA
            0x03 => {
                self.acc = self.sp;
                CpuStatus::Running
            }
            // MovFlgA
            0x04 => {
                let status = self.status & 0x000f;
                let acc_msb = self.acc & 0xff00;
                self.acc = acc_msb | status;
                CpuStatus::Running
            }
            // MovAFlg
            0x05 => {
                self.acc = self.status;
                CpuStatus::Running
            }
            // Notr
            op @ 0x06..=0x07 => {
                let reg = self.reg1(op);
                let new  = !*reg;
                *reg = new;

                self.n((new as i16) < 0);
                self.z(new == 0);
                CpuStatus::Running
            }
            // Negr
            op @ 0x08..=0x09 => {
                let reg = self.reg1(op);
                let old = *reg as i16;
                let (new, overflow) = old.overflowing_neg();
                *reg = new as u16;

                self.n(new < 0);
                self.z(new == 0);
                self.v(overflow);
                CpuStatus::Running
            }
            // Aslr
            op @ 0x0a..=0x0b => {
                let reg = self.reg1(op);
                let old = *reg;
                let new = old << 1;
                *reg = new;

                self.n(new & 0x8000 != 0);
                self.z(new == 0);
                self.v(old & 0x8000 == 0 && new & 0x8000 != 0);
                self.c(old & 0x8000 > 0);
                CpuStatus::Running
            }
            // Asrr
            op @ 0x0c..=0x0d => {
                let reg = self.reg1(op);
                let old = *reg;
                let new = if old & 0x8000 > 0 {
                    (old >> 1) | 0x8000
                } else {
                    old >> 1
                };
                *reg = new;

                self.n((new as i16) < 0);
                self.z(new == 0);
                self.c(old & 0x0001 > 0);
                CpuStatus::Running
            }
            // Rolr
            op @ 0x0e..=0x0f => {
                let old_carry = self.status & STAT_C;
                let reg = self.reg1(op);
                let old = *reg;
                let new = (old << 1) | old_carry;
                let new_carry = old & 0x8000;

                *reg = new;

                self.c(new_carry > 1);
                CpuStatus::Running
            }
            //Rorr
            op @ 0x10..=0x11 => {
                let old_carry = self.status & STAT_C;
                let reg = self.reg1(op);
                let old = *reg;
                let new = (old >> 1) | (old_carry << 7);
                let new_carry = old & 0x0001;

                *reg = new;

                self.c(new_carry > 1);
                CpuStatus::Running
            }
            // Br
            op @ 0x12..=0x13 => {
                self.br(op);
                CpuStatus::Running
            }
            // brle
            op @ 0x14..=0x15 => {
                if (self.status & STAT_N > 0) || (self.status & STAT_Z > 0) {
                    self.br(op);
                }
                CpuStatus::Running
            }
            // brlt
            op @ 0x16..=0x17 => {
                if self.status & STAT_N > 0 {
                    self.br(op);
                }
                CpuStatus::Running
            }
            // breq
            op @ 0x18..=0x19 => {
                if self.status & STAT_Z > 0 {
                    self.br(op);
                }
                CpuStatus::Running
            }
            // brne
            op @ 0x1a..=0x1b => {
                if self.status & STAT_Z == 0 {
                    self.br(op);
                }
                CpuStatus::Running
            }
            // brge
            op @ 0x1c..=0x1d => {
                if self.status & STAT_N == 0 {
                    self.br(op);
                }
                CpuStatus::Running
            }
            // brgt
            op @ 0x1e..=0x1f => {
                if (self.status & STAT_N == 0) && (self.status & STAT_Z == 0) {
                    self.br(op);
                }
                CpuStatus::Running
            }
            // brv
            op @ 0x20..=0x21 => {
                if self.status & STAT_V > 0 {
                    self.br(op);
                }
                CpuStatus::Running
            }
            // brc
            op @ 0x22..=0x23 => {
                if self.status & STAT_C > 0 {
                    self.br(op);
                }
                CpuStatus::Running
            }
            // call
            op @ 0x24..=0x25 => {
                self.sp -= 2;
                match self.mem.store_word(self.sp, self.pc) {
                    Ok(()) => {
                        self.br(op);
                        CpuStatus::Running
                    }
                    Err(()) => CpuStatus::Faulted,
                }
            }
            // Nopn, Nop, Deci, Deco, Hexo, Stro
            0x26..=0x4f => {
                // use the `?` operator to handle faults within function
                fn trap_ops(cpu: &mut Cpu) -> Result<(), ()> {
                    let temp = cpu.mem.word(0xfff6);
                    cpu.mem.store_byte(temp - 1, cpu.op)?;
                    cpu.mem.store_word(temp - 3, cpu.sp)?;
                    cpu.mem.store_word(temp - 5, cpu.pc)?;
                    cpu.mem.store_word(temp - 7, cpu.indx)?;
                    cpu.mem.store_word(temp - 9, cpu.acc)?;
                    cpu.mem.store_word(temp - 10, (cpu.status << 8) & 0x0f00)?;
                    cpu.sp = temp - 10;
                    cpu.pc = cpu.mem.word(0xfffe);
                    Ok(())
                }

                match trap_ops(self) {
                    Ok(()) => CpuStatus::Running,
                    Err(()) => CpuStatus::Faulted,
                }
            }
            // AddSP
            op @ 0x50..=0x57 => {
                self.sp += self.word_at(AddrMode::of3(op));
                CpuStatus::Running
            }
            // SubSP
            op @ 0x58..=0x5f => {
                self.sp -= self.word_at(AddrMode::of3(op));
                CpuStatus::Running
            }
            // Addr
            op @ 0x60..=0x6f => {
                let operand = self.word_at(AddrMode::of3(op));
                let reg = self.reg4(op);
                let old = *reg;
                let (new, overflow) = (old as i16)
                    .overflowing_add(operand as i16);
                let (_, carry) = old.overflowing_add(operand);

                *reg = new as u16;

                self.n((new as i16) < 0);
                self.z(new == 0);
                self.v(overflow);
                self.c(carry);
                CpuStatus::Running
            }
            // Subr
            op @ 0x70..=0x7f => {
                let operand = self.word_at(AddrMode::of3(op));
                let reg = self.reg4(op);
                let old = *reg;
                let (new, overflow) = (old as i16)
                    .overflowing_sub(operand as i16);
                let(_, carry) = old.overflowing_sub(operand);

                *reg = new as u16;

                self.n(new < 0);
                self.z(new == 0);
                self.v(overflow);
                self.c(carry);
                CpuStatus::Running
            }
            // Andr
            op @ 0x80..=0x8f => {
                let operand = self.word_at(AddrMode::of3(op));
                let reg = self.reg4(op);
                let new = *reg & operand;
                *reg = new;

                self.n((new as i16) < 0);
                self.z(new == 0);
                CpuStatus::Running
            }
            // Orr
            op @ 0x90..=0x9f => {
                let operand = self.word_at(AddrMode::of3(op));
                let reg = self.reg4(op);
                let new = *reg | operand;
                *reg = new;

                self.n((new as i16) < 0);
                self.z(new == 0);
                CpuStatus::Running
            }
            // Cpwr
            op @ 0xa0..=0xaf => {
                let val = *self.reg4(op);
                let operand = self.word_at(AddrMode::of3(op));
                let (temp, overflow) = (val as i16)
                    .overflowing_sub(operand as i16);
                let (_, carry) = val.overflowing_sub(operand);

                self.n(temp < 0);
                self.z(temp == 0);
                self.v(overflow);
                self.c(carry);
                CpuStatus::Running
            }
            // Cpbr
            op @ 0xb0..=0xbf => {
                let val = *self.reg4(op) as u8;
                let operand = self.byte_at(AddrMode::of3(op));
                let (temp, _) = (val as i16)
                    .overflowing_sub(operand as i16);

                self.n(temp < 0);
                self.z(temp == 0);
                self.v(false);
                self.c(false);
                CpuStatus::Running
            }
            // Ldwr
            op @ 0xc0..=0xcf => {
                let word = self.word_at(AddrMode::of3(op));
                let reg = self.reg4(op);
                *reg = word;

                self.n((word as i16) < 0);
                self.z(word == 0);
                CpuStatus::Running
            }
            // Ldbr
            op @ 0xd0..=0xdf => {
                let byte = match AddrMode::of3(op) {
                    AddrMode::Immediate => self.byte_at(AddrMode::Immediate),
                    mode => {
                        let addr = self.opspec_addr(mode);

                        // addr of input device is at 0xfff8
                        if addr == self.mem.word(0xfff8) {
                            if let Some(c) = self.input.pop() {
                                c
                            } else {
                                // When this happens we need to rewind the cpu
                                // so that the ldb happens again when run() is
                                // called.
                                self.pc -= 3;
                                return (CpuStatus::BlockedInput, None);
                            }
                        } else {
                            self.mem[addr]
                        }
                    }
                };
                let reg = self.reg4(op);
                *reg &= 0xff00;
                *reg |= byte as u16;

                self.n(false);
                self.z(byte == 0);
                CpuStatus::Running
            }
            // Stwr
            op @ 0xe0..=0xef => {
                match AddrMode::of3(op) {
                    AddrMode::Immediate => CpuStatus::Faulted,
                    mode => {
                        let addr = self.opspec_addr(mode);
                        let val = *self.reg4(op);
                        match self.mem.store_word(addr, val) {
                            Ok(()) => CpuStatus::Running,
                            Err(()) => CpuStatus::Faulted,
                        }
                    }
                }
            }
            //Stbr
            op @ 0xf0..=0xff => {
                match AddrMode::of3(op) {
                    AddrMode::Immediate => CpuStatus::Faulted,
                    mode => {
                        let to_store = *self.reg4(op) as u8;
                        let addr = self.opspec_addr(mode);

                        // addr of output device is at fffa
                        if addr == self.mem.word(0xfffa) {
                            out_byte = Some(to_store);
                            CpuStatus::Running
                        } else {
                            match self.mem.store_byte(addr, to_store) {
                                Ok(()) => CpuStatus::Running,
                                Err(()) => CpuStatus::Faulted,
                            }
                        }
                    }
                }
            }
        };
        (stat, out_byte)
    }

    fn op_is_unary(&self) -> bool {
        match self.op {
            0x00..=0x11 | 0x26 | 0x27 => true,
            _ => false,
        }
    }

    // register spec in the 1st bit
    fn reg1(&mut self, instr: u8) -> &mut u16 {
        self.reg(instr & 0x01)
    }

    // register spec in the 4th bit
    fn reg4(&mut self, instr: u8) -> &mut u16 {
        self.reg((instr & 0x08) >> 3)
    }

    fn reg(&mut self, spec: u8) -> &mut u16 {
        match spec {
            0x00 => &mut self.acc,
            0x01 => &mut self.indx,
            spec => panic!("invalid register spec: {}", spec),
        }
    }

    fn byte_at(&self, mode: AddrMode) -> u8 {
        match mode {
            AddrMode::Immediate => self.opspec as u8,
            _ => self.mem[self.opspec_addr(mode)],
        }
    }

    // Using `self.opspec`, get a value using the given addrmode
    fn word_at(&self, mode: AddrMode) -> u16 {
        match mode {
            AddrMode::Immediate => self.opspec,
            _ => self.mem.word(self.opspec_addr(mode)),
        }
    }

    // Panics if called with AddrMode::Immediate
    fn opspec_addr(&self, mode: AddrMode) -> u16 {
        match mode {
            AddrMode::Direct => self.opspec,
            AddrMode::Indirect => self.mem.word(self.opspec),
            AddrMode::StackRelative =>
                self.sp.overflowing_add(self.opspec).0,
            AddrMode::StackRelativeDeferred =>
                self.mem.word(self.sp.overflowing_add(self.opspec).0),
            AddrMode::Indexed => self.indx.overflowing_add(self.opspec).0,
            AddrMode::StackIndexed =>
                self.sp.overflowing_add(self.indx).0
                    .overflowing_add(self.opspec).0,
            AddrMode::StackDeferredIndexed => {
                let (stack_addr, _) = self.sp.overflowing_add(self.opspec);
                self.mem.word(stack_addr)
                    .overflowing_add(self.indx).0
            }
            _ => unreachable!(),
        }
    }

    // helper to reduce repitition
    fn br(&mut self, op: u8) {
        self.pc = self.word_at(AddrMode::of1(op));
    }

    fn n(&mut self, new_n: bool) {
        self.set_stat(STAT_N, new_n);
    }

    fn z(&mut self, new_z: bool) {
        self.set_stat(STAT_Z, new_z);
    }

    fn v(&mut self, new_v: bool) {
        self.set_stat(STAT_V, new_v);
    }

    fn c(&mut self, new_c: bool) {
        self.set_stat(STAT_C, new_c);
    }

    fn set_stat(&mut self, bit: u16, new_bit: bool) {
        if new_bit {
            self.status |= bit;
        } else {
            self.status &= !bit;
        }
    }

    pub fn reg_dump(&self) -> String {
        format!("  \
            N={}  Z={}  V={}  C={}\n\
            Accumulator:      {:04x}\n\
            Index:            {:04x}\n\
            Program Counter:  {:04x}\n\
            Stack Pointer:    {:04x}\n\
            Instr Reg (Op):   {:02x}\n\
            Instr Reg (Spec): {:04x}\n",
            (self.status & STAT_N) >> 3,
            (self.status & STAT_Z) >> 2,
            (self.status & STAT_V) >> 1,
            self.status & STAT_C,
            self.acc,
            self.indx,
            self.pc,
            self.sp,
            self.op,
            self.opspec,
        )
    }

    pub fn mem_dump(&self) -> String {
        format!("{}", self.mem)
    }
}

impl fmt::Debug for Cpu {
    /// Gives `self.reg_dump()`, the input formatted as a hex byte vec, and
    /// a memory dump including the ROM.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(&self.reg_dump())?;
        writeln!(f, "Input: {:02x?}", self.input)?;
        f.write_fmt(format_args!("{:?}", self.mem))
    }
}

impl fmt::Display for Cpu {
    /// Gives `self.reg_dump()`, an ascii representation of the input, and a
    /// memory dump without the ROM.
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(&self.reg_dump())?;
        writeln!(f, "Input: `{}`", crate::ascii_repr(&self.input))?;
        f.write_fmt(format_args!("{}", self.mem))
    }
}

#[cfg(test)]
mod unittest {
    use super::*;
    use super::AddrMode::*;

    fn addrmode_of(op: u8) -> AddrMode {
        match op & 0x07 {
            0 => Immediate,
            1 => Direct,
            2 => Indirect,
            3 => StackRelative,
            4 => StackRelativeDeferred,
            5 => Indexed,
            6 => StackIndexed,
            7 => StackDeferredIndexed,
            _ => unreachable!(),
        }
    }

    #[test]
    fn conv_addrmode() {
        for op in 0..255 {
            assert_eq!(addrmode_of(op), AddrMode::of3(op));
        }
    }
}

