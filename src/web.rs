#![cfg(feature = "web")]

use std::ffi::OsStr;
use std::io::Write;
use std::iter;

use ariadne::Source;
use clap::{Arg, ColorChoice, Command, Parser};
use clap::{
    error::ErrorKind,
    builder::TypedValueParser,
};
use wasm_bindgen::prelude::*;

use crate::asm;
use crate::cpu::{Cpu, CpuStatus};

mod js {
    use wasm_bindgen::prelude::*;

    #[wasm_bindgen]
    extern "C" {
        #[wasm_bindgen(js_namespace = console)]
        pub fn log(msg: &str);
        #[wasm_bindgen(js_namespace = console)]
        pub fn warn(msg: &str);
        #[wasm_bindgen(js_namespace = console)]
        pub fn error(msg: &str);
    }
}

pub fn asm(code: &str) -> Result<Vec<u8>, Vec<u8>> {
    match asm::asm(code) {
        Ok(obj) => Ok(obj.into()),
        Err(errs) => {
            let mut rslt = Vec::new();
            for err in errs {
                err.report("editor")
                    .write(("editor", Source::from(code)), &mut rslt)
                    .expect("failed to write errors to buffer");
            }
            Err(rslt)
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct U16AddressParser;

impl TypedValueParser for U16AddressParser {
    type Value = u16;

    fn parse_ref(
        &self,
        _cmd: &Command,
        arg: Option<&Arg>,
        value: &OsStr,
    ) -> Result<u16, clap::Error> {
        let value = value.to_str()
            .ok_or(clap::Error::raw(
                ErrorKind::InvalidValue,
                "string was not valid unicode",
            ))?;

        if value.len() < 2 {
            i16::from_str_radix(value, 10)
                .map(|addr| addr as u16 )
        } else {
            let base = match &value[..2] {
                "0x" => 16,
                "0o" => 8,
                "0b" => 2,
                _ => 10,
            };

            match base {
                base @ (2 | 8 | 16) => {
                    u16::from_str_radix(&value[2..], base)
                }
                10 => match value.chars().next().expect("checked") {
                    '-' => i16::from_str_radix(value, 10)
                        .map(|addr| addr as u16 ),
                    _ => u16::from_str_radix(value, 10),
                }
                _ => unreachable!(),
            }
        }.map_err(|e| {
            let msg = if let Some(arg) = arg {
                format!("failed to parse `{}` for {}: {:#}", value, arg, e)
            } else {
                format!("failed to parse `{}`: {:#}", value, e)
            };
            clap::Error::raw(ErrorKind::InvalidValue, msg)
        })
    }
}

#[derive(Debug, Parser)]
#[command(
    name = "Pep9 Web Cli",
    author = "Wesley Hershberger <mggmugginsmc@gmail.com>",

    bin_name = "",
    disable_help_flag = true,
    color = ColorChoice::Always,
)]
enum Cli {
    /// Assemble the code in the editor and print the object code
    Asm,

    /// Assemble and run the code in the editor
    Run,

    /// Assemble the code in the editor, and run the loader, but don't start
    /// the cpu
    Load,

    /// Continue debugging
    ///
    /// Alias `cont`
    #[command(alias = "cont")]
    Continue,

    /// Run one instruction
    Step,

    /// Manipulate breakpoints
    ///
    /// If run with no options, sets a breakpoint at each ADDR. Alias `bp`
    #[command(alias = "bp")]
    Breakpoint {
        /// Remove the breakpoint(s) ADDR instead of setting them. Alias `rm`
        #[arg(
            short = 'r',
            long = "remove",
            alias = "rm",
            requires = "ADDR",
        )]
        remove: bool,

        /// Show all breakpoints
        #[arg(short, long, conflicts_with = "remove")]
        list: bool,

        /// Breakpoint(s) to manipulate
        ///
        /// Breakpoints are numeric values that fit in 16 bits. They may be
        /// specified as signed or unsigned decimal, or as unsigned binary,
        /// octal, or hexidecimal using the prefixes `0b`, `0o`, or `0x`
        /// respectively
        #[arg(
            name = "ADDR",
            value_parser = U16AddressParser,
            required = false,
        )]
        addrs: Vec<u16>,
    },

    /// Add input to the buffer
    Input {
        /// Show the current buffer instead of adding input
        #[arg(short, long)]
        list: bool,

        /// Pass input on the command line instead of via stdin
        ///
        /// This is useful when you don't want to append a newline to the input
        #[arg(value_parser, conflicts_with = "list")]
        input: Option<String>,
    },

    /// Print the Cpu state
    Cpu,

    /// Print a memory dump
    #[command(alias = "mem")]
    Memory {
        /// Include ROM in the memory dump
        #[arg(short, long)]
        all: bool,
    },
}

#[derive(Clone, Copy, Debug)]
#[wasm_bindgen]
pub enum RsltKind {
    None,

    Txt,

    RunCpu,

    FetchInput,
}

/// A sad fascimile of a tagged union for the benefit of JS...
#[derive(Debug)]
#[wasm_bindgen]
pub struct CliRslt {
    #[wasm_bindgen(getter_with_clone)]
    pub txt: Vec<u8>,

    pub kind: RsltKind,
}

impl CliRslt {
    fn none() -> CliRslt {
        CliRslt {
            txt: vec![],
            kind: RsltKind::None,
        }
    }

    fn txt(txt: Vec<u8>) -> CliRslt {
        CliRslt {
            txt,
            kind: RsltKind::Txt,
        }
    }

    fn msg(msg: &str) -> CliRslt {
        let mut txt = msg.as_bytes().to_owned();

        match msg.chars().last() {
            Some('\n') | None => {},
            _ => txt.push(b'\n'),
        }

        CliRslt {
            txt,
            kind: RsltKind::Txt,
        }
    }

    fn msgs(msgs: Vec<String>) -> CliRslt {
        let mut txt = msgs.join("\n")
            .as_bytes()
            .to_owned();

        if txt.len() > 0 {
            txt.push(b'\n');
        }

        CliRslt {
            txt,
            kind: if msgs.len() == 0 {
                RsltKind::None
            } else {
                RsltKind::Txt
            },
        }
    }

    fn run_cpu() -> CliRslt {
        CliRslt {
            txt: vec![],
            kind: RsltKind::RunCpu,
        }
    }

    fn fetch_input() -> CliRslt {
        CliRslt {
            txt: vec![],
            kind: RsltKind::FetchInput,
        }
    }
}

/// This has got to be the dumbest API I have ever written. The things I do for
/// JavaScript...
#[wasm_bindgen]
pub struct Pep9Shell {
    cpu: Cpu,
    status: CpuStatus,
}

#[wasm_bindgen]
impl Pep9Shell {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Pep9Shell {
        Pep9Shell {
            cpu: Cpu::new(),
            status: CpuStatus::Stopped,
        }
    }

    /// Interpret and execute a command
    pub fn cmd(&mut self, cmd: &str, editor: &str) -> CliRslt {
        let args = iter::once("pep9_web_cli")
            .chain(cmd.split_whitespace());

        match Cli::try_parse_from(args) {
            Ok(cli) => self.exec_cli(cli, editor),
            Err(err) => {
                let mut rslt = vec![];
                write!(rslt, "{}\n", err.render().ansi())
                    .expect("write to vec can't fail");
                CliRslt::txt(rslt)
            }
        }
    }

    pub fn run(&mut self) -> Vec<u8> {
        let (new_status, output) = self.cpu.run();
        self.status = new_status;
        output
    }

    pub fn input(&mut self, input: &str) {
        self.cpu.input(input.as_bytes());
    }

    pub fn is_finished(&self) -> bool {
        match self.status {
            CpuStatus::Stopped |
            CpuStatus::Faulted |
            CpuStatus::Breakpoint(_) => true,
            CpuStatus::Running |
            CpuStatus::BlockedInput => false,
        }
    }
}

impl Pep9Shell {
    fn exec_cli(&mut self, cli: Cli, editor: &str) -> CliRslt {
        let mut obj = match asm(editor) {
            Ok(obj) => obj,
            Err(err) => return CliRslt::txt(err),
        };

        match cli {
            Cli::Asm => {
                obj.push(b'\n');
                CliRslt::txt(obj)
            }
            Cli::Run => {
                self.cpu.clear_input();
                self.cpu.input(&obj);
                self.cpu.load();

                CliRslt::run_cpu()
            }
            Cli::Load => {
                self.cpu.clear_input();
                self.cpu.input(&obj);
                self.cpu.load();

                CliRslt::none()
            }
            Cli::Continue => {
                CliRslt::run_cpu()
            }
            Cli::Step => {
                let (status, maybe_byte) = self.cpu.step();
                self.status = status;

                if let Some(byte) = maybe_byte {
                    CliRslt::txt(vec![byte])
                } else {
                    CliRslt::none()
                }
            }
            Cli::Breakpoint { addrs, list: false, remove: false } => {
                if addrs.len() == 0 {
                    let mut points = self.cpu.breakpoints.iter()
                        .map(|point| format!("0x{:04x}", point) )
                        .collect::<Vec<_>>();

                    points.sort();

                    CliRslt::msgs(points)
                } else {
                    for addr in addrs {
                        self.cpu.breakpoints.insert(addr);
                    }

                    CliRslt::none()
                }
            }
            // Show all the breakpoints
            Cli::Breakpoint { addrs, list: true, .. } if addrs.len() == 0 => {
                let mut points = self.cpu.breakpoints.iter()
                    .map(|point| format!("0x{:04x}", point) )
                    .collect::<Vec<_>>();

                points.sort();

                CliRslt::msgs(points)
            }
            // Show only the requested breakpoints
            Cli::Breakpoint { addrs, list: true, .. } => {
                let mut breakpoints = Vec::with_capacity(addrs.len());

                for addr in addrs {
                    if self.cpu.breakpoints.contains(&addr) {
                        breakpoints.push(format!("0x{:04x}", addr));
                    }
                }

                CliRslt::msgs(breakpoints)
            }
            Cli::Breakpoint { addrs, remove: true, .. } => {
                let mut unset = vec![];

                for addr in addrs {
                    if !self.cpu.breakpoints.remove(&addr) {
                        unset.push(
                            format!("breakpoint wasn't set: {:04x}", addr)
                        );
                    }
                }

                CliRslt::msgs(unset)
            }
            Cli::Input { input: Some(txt), list: false } => {
                self.input(&txt);
                CliRslt::none()
            }
            Cli::Input { input: None, list: false } => {
                CliRslt::fetch_input()
            }
            Cli::Input { list: true, .. } => {
                CliRslt::txt(self.cpu.input_bytes().collect())
            }
            Cli::Cpu => {
                CliRslt::msg(&self.cpu.reg_dump())
            }
            Cli::Memory { all: false } => {
                CliRslt::msg(&self.cpu.mem_dump())
            }
            Cli::Memory { all: true } => {
                CliRslt::msg(&self.cpu.mem_dump())
            }
        }
    }
}

/// Call this function once during initialization and we'll get better error
/// messages when the module panics.
#[wasm_bindgen]
pub fn set_panic_hook() {
    console_error_panic_hook::set_once();
}

#[cfg(test)]
mod test {
    use std::ffi::OsStr;
    use clap::Command;
    use super::*;

    #[test]
    fn test_u16_addr_parser() {
        let parse = |val: &str| {
            U16AddressParser.parse_ref(
                &Command::new(""),
                None,
                &OsStr::new(val),
            )
        };

        let parse_ok = |val|
            parse(val).expect("parse_ok was Err");

        assert_eq!(0, parse_ok("0"));
        assert_eq!(14, parse_ok("14"));
        assert_eq!((-14i16) as u16, parse_ok("-14"));
        assert_eq!(0b1001, parse_ok("0b1001"));
        assert_eq!(0o755, parse_ok("0o755"));
        assert_eq!(0xe, parse_ok("0xe"));
        assert_eq!(0xbeef, parse_ok("0xbeef"));

        assert!(parse("0xdeadbeef").is_err());
        assert!(parse("--14").is_err());
        assert!(parse("0b32").is_err());
        assert!(parse("0o9").is_err());
        assert!(parse("0x").is_err());
    }
}
