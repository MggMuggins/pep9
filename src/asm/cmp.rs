#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum ObjEq {
    Start,

    ConsumedNybble1,
    ConsumedNybble2,
    ConsumedSkip,

    Err,

    Sentinel1,
    Sentinel2,
}

impl ObjEq {
    fn change(&mut self, c1: char, c2: char) {
        let chars_equal = c1.eq_ignore_ascii_case(&c2);
        let sentinel = c1 == 'z';

        *self = match self {
            ObjEq::Start | ObjEq::ConsumedSkip => if chars_equal {
                ObjEq::ConsumedNybble1
            } else {
                eprintln!("{:?}: {} == {}", self, c1, c2);
                ObjEq::Err
            }
            ObjEq::ConsumedNybble1 => if sentinel {
                ObjEq::Sentinel1
            } else if chars_equal {
                ObjEq::ConsumedNybble2
            } else {
                eprintln!("{:?}: {} == {}", self, c1, c2);
                ObjEq::Err
            }
            ObjEq::ConsumedNybble2 => ObjEq::ConsumedSkip,
            ObjEq::Sentinel1 => if sentinel {
                ObjEq::Sentinel2
            } else {
                eprintln!("{:?}: {} == {}", self, c1, c2);
                ObjEq::Err
            }
            ObjEq::Err => ObjEq::Err,
            ObjEq::Sentinel2 => ObjEq::Sentinel2,
        };
    }

    fn done(&self) -> bool {
        match self {
            ObjEq::Err | ObjEq::Sentinel2 => true,
            _ => false,
        }
    }
}

/// Convenience function to compare two object code strings. This function
/// follows the semantics of the default Pep9 loader (Warford 473)
pub fn obj_eq(obj1: &str, obj2: &str) {
    let pairs = obj1.chars()
        .zip(obj2.chars());

    let mut state = ObjEq::Start;
    for (c1, c2) in pairs {
        if state.done() {
            break;
        }

        state.change(c1, c2);
    }

    if let ObjEq::Err = state {
        let pairs = obj1.chars()
            .zip(obj2.chars())
            .collect::<Vec<_>>();

        eprintln!("{:02x?}", pairs);
        panic!("obj_eq: mismatched object code!");
    }
}

