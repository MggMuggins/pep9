use std::fmt;
use std::iter;
use std::mem;
use std::ops::Range;

use chumsky::combinator::Repeated;
use chumsky::prelude::*;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Dotop {
    Addrss,
    Align,
    Ascii,
    Block,
    Burn,
    Byte,
    End,
    Equate,
    Word,
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub enum TokenType {
    Ident(String),

    Dotop(Dotop),
    LabelDef(String),

    // literals
    Num(u16),
    Str(String),
    Char(u8),

    Newline,
    Comma,
    Space,

    Comment(String),
}

impl TokenType {
    pub fn typ_eq(&self, other: &TokenType) -> bool {
        mem::discriminant(self) == mem::discriminant(other)
    }

    // 1-1 correspondence with the variant name/discriminant
    pub fn typ_name(&self) -> &'static str {
        match self {
            TokenType::Ident(_) => "identifier",
            TokenType::Dotop(_) => "pseudo-op",
            TokenType::LabelDef(_) => "label definition",
            TokenType::Num(_) => "number",
            TokenType::Str(_) => "string",
            TokenType::Char(_) => "character",
            TokenType::Newline => "`\\n`",
            TokenType::Comma => "`,`",
            TokenType::Space => "` `",
            TokenType::Comment(_) => "comment",
        }
    }

    // infer a little extra info about what expecting self means
    pub fn expected_name(&self) -> &'static str {
        match self {
            TokenType::Ident(s) if s.len() == 0 => "identifier",
            // crimes
            // mnemonics will show up as an all-caps ident, addrmodes won't
            TokenType::Ident(s) if s.chars().all(|c| c.is_ascii_uppercase() ) =>
                "mnemonic",
            TokenType::Ident(s) if s.chars().all(|c| c.is_ascii_lowercase() ) =>
                "address mode",
            _ => self.typ_name(),
        }
    }
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        f.write_str(self.expected_name())
    }
}

#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct Token {
    pub typ: TokenType,
    pub span: Range<usize>,
}

fn maybe_space() -> Repeated<impl Parser<char, (), Error = Simple<char>>> {
    choice((just(' '), just('\t'), just('\r')))
        .ignored()
        .repeated()
}

fn space() -> impl Parser<char, Token, Error = Simple<char>> {
    maybe_space()
        .at_least(1)
        .map_with_span(|_, span| Token { typ: TokenType::Space, span } )
}

fn number_base(
    prefix: &'static str,
    radix: u32,
    max_digits: usize,
) -> impl Parser<char, u16, Error = Simple<char>> {
    just(prefix)
        .ignore_then(
            filter(move |c: &char| c.is_digit(radix) )
                .repeated()
                .collect()
        )
        .validate(move |txt: String, span, emit| {
            match u16::from_str_radix(&txt, radix) {
                Ok(val) => {
                    if txt.len() > max_digits {
                        emit(Simple::custom(span,
                            format!(
                                "{} numbers must have {} digits or less",
                                prefix,
                                max_digits,
                            ))
                        );
                    }
                    val
                }
                Err(_) => {
                    emit(Simple::custom(span,
                        format!("invalid number: `{}`", txt)));
                    0
                }
            }
        })
}

fn number_decimal()
    -> impl Parser<char, u16, Error = Simple<char>> {
    just('-')
        .repeated()
        .at_most(1)
        .then(
            filter(|c: &char| c.is_ascii_digit() )
                .repeated()
                .at_least(1)
                .collect::<String>()
        )
        .validate(move |(neg, txt), span: Range<usize>, emit| {
            match u16::from_str_radix(&txt, 10) {
                Ok(num) => if neg.len() == 0 {
                    num
                } else {
                    (-(num as i16)) as u16
                },
                Err(err) => {
                    emit(Simple::custom(span, err));
                    0
                }
            }
        })
}

fn number() -> impl Parser<char, Token, Error = Simple<char>> {
    choice((
        number_base("0b", 2, 16),
        number_base("0o", 8, 8),
        number_base("0x", 16, 4),
        number_decimal(),
    ))
        .map_with_span(|num, span| Token { typ: TokenType::Num(num), span })
}

fn character() -> impl Parser<char, u8, Error = Simple<char>> {
    choice((
        just('\\')
            .ignore_then(choice((
                just('n').to(b'\n'),
                just('t').to(b'\t'),
                just('\\').to(b'\\'),
                just('\'').to(b'\''),
                just('"').to(b'"'),
                just('x')
                    .ignore_then(filter(|c: &char| c.is_digit(16) )
                        .repeated()
                        .exactly(2)
                        .map(|digits|
                            u8::from_str_radix(
                                &digits.iter().collect::<String>(),
                                16
                            ).expect("filter() ensures parse can occur")
                        )
                    ),
            ))),
        filter_map(|span, c: char| if !c.is_ascii() {
            Err(Simple::custom(span,
                format!("'{}' is not an ascii character", c)))
        } else if c == '"' {
            // Since this rule is used for Ascii chars inside string literals,
            // it ought to not consume any actual `"`. char_lit handles them
            // correctly
            Err(Simple::expected_input_found(span, None, Some('"')))
        } else {
            // Note that we do allow `'''` as a valid char literal
            Ok(c as u8)
        }),
    ))
}

fn char_lit() -> impl Parser<char, Token, Error = Simple<char>> {
    choice((
        character(),
        just('"').to(b'"'),
    ))
        .delimited_by(just('\''), just('\''))
        .map_with_span(|c, span| Token { typ: TokenType::Char(c), span })
}

fn dotop(name: &'static str, op: Dotop)
    -> impl Parser<char, Token, Error = Simple<char>>
{
    just('.')
        .then(just_ignore_case(name))
        .map_with_span(move |_, span|
            Token { typ: TokenType::Dotop(op), span }
        )
}

// Caveat: `word` must be uppercase
fn just_ignore_case(word: &'static str)
    -> impl Parser<char, String, Error = Simple<char>>
{
    filter(move |c: &char| (&word).contains(c.to_ascii_uppercase()) )
        .repeated()
        .exactly(word.len())
        .collect()
        .try_map(move |s: String, span: Range<usize>| {
            if s.eq_ignore_ascii_case(&word) {
                Ok(s)
            } else {
                Err(Simple::expected_input_found(
                    span.clone(),
                    iter::once(word[span.clone()].chars().next()),
                    s[span].chars().next(),
                ))
            }
        })
}

fn tokenizer() -> impl Parser<char, Vec<Token>, Error = Simple<char>> {
    let ascii = character()
        .repeated()
        .delimited_by(just('"'), just('"'))
        .map_with_span(|vec, span| {
            let s = String::from_utf8(vec)
                .expect("parser has ensured all bytes are ascii");
            Token { typ: TokenType::Str(s), span }
        });

    let ident = text::ident()
        .map_with_span(|ident, span|
            Token { typ: TokenType::Ident(ident), span }
        );

    let label_def = text::ident()
        .then_ignore(just(':'))
        .map_with_span(|lab, span|
            Token { typ: TokenType::LabelDef(lab), span }
        );

    let dotops = choice((
        dotop("ADDRSS", Dotop::Addrss),
        dotop("ALIGN", Dotop::Align),
        dotop("ASCII", Dotop::Ascii),
        dotop("BLOCK", Dotop::Block),
        dotop("BURN", Dotop::Burn),
        dotop("BYTE", Dotop::Byte),
        dotop("EQUATE", Dotop::Equate),
        dotop("WORD", Dotop::Word),
    ));

    choice((
        none_of("\n").repeated()
            .delimited_by(just(';'), just('\n'))
            .collect()
            .map_with_span(|txt, span|
                Token { typ: TokenType::Comment(txt), span }
            )
            .labelled("comment"),
        just(',').map_with_span(|_, span|
            Token { typ: TokenType::Comma, span }
        ),
        just('\n').map_with_span(|_, span|
            Token { typ: TokenType::Newline, span }
        ),
        space().labelled("horizontal whitespace"),

        ascii.labelled("string"),
        number().labelled("number"),
        char_lit().labelled("character literal"),

        dotops.labelled("pseudo-op"),
        label_def.labelled("label definition"),
        ident.labelled("identifier"),
    ))
        //.recover_with(skip_then_retry_until([]))
        .repeated()
        .then(choice((
            dotop("END", Dotop::End),
            //Nasty hack
            end().map_with_span(|_, span| Token { typ: TokenType::Space, span }),
        )))
        .map(|(mut tokens, end)| {
            tokens.push(end);
            tokens
        })
}

pub fn tokens(asm: &str) -> (Option<Vec<Token>>, Vec<Simple<char>>) {
    //TODO: TokenType::Err
    tokenizer().parse_recovery(asm)
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn exact_tokens() {
        let cases = vec![
            ("ident", TokenType::Ident("ident".to_string())),
            ("lab:", TokenType::LabelDef("lab".to_string())),

            (".aDDrss", TokenType::Dotop(Dotop::Addrss)),
            (".ALIGN", TokenType::Dotop(Dotop::Align)),
            (".ascII", TokenType::Dotop(Dotop::Ascii)),
            (".bLoCk", TokenType::Dotop(Dotop::Block)),
            (".BuRn", TokenType::Dotop(Dotop::Burn)),
            (".bytE", TokenType::Dotop(Dotop::Byte)),
            (".eNd", TokenType::Dotop(Dotop::End)),
            (".eQuAtE", TokenType::Dotop(Dotop::Equate)),
            (".WoRd", TokenType::Dotop(Dotop::Word)),

            ("67", TokenType::Num(67)),
            ("-23", TokenType::Num(-23i16 as u16)),
            ("0b1101", TokenType::Num(0b1101)),
            ("0o071", TokenType::Num(0o071)),
            ("0xbeef", TokenType::Num(0xbeef)),

            (r#""a str\x20\t\n\" cool""#,
                TokenType::Str("a str\x20\t\n\" cool".to_string())),

            ("'c'", TokenType::Char(b'c')),
            ("' '", TokenType::Char(b' ')),
            ("'\\\\'", TokenType::Char(b'\\')),
            ("'\\t'", TokenType::Char(b'\t')),
            ("'\\n'", TokenType::Char(b'\n')),
            ("'\\x5a'", TokenType::Char(b'\x5a')),
            ("'\\\"'", TokenType::Char(b'"')),
            ("'\\\"'", TokenType::Char(b'"')),

            ("\n", TokenType::Newline),
            (",", TokenType::Comma),
            ("  \r  \t", TokenType::Space),
            ("; Comments! \n",
                TokenType::Comment(" Comments! ".to_string())),
        ];

        for (case, expected) in cases {
            eprintln!("`{}` expect {:?}", case.escape_debug(), expected);
            let tokens = tokens(case).0
                .expect("encountered parse error");

            match &tokens[..] {
                [end] => assert_eq!(TokenType::Dotop(Dotop::End), end.typ),
                // With no .end, the tokenizer should insert a space at eof
                [tok, space] => {
                    assert_eq!(expected, tok.typ);
                    assert_eq!(TokenType::Space, space.typ);
                },
                _ => panic!("got unexpected number of tokens"),
            }
        }
    }
}

