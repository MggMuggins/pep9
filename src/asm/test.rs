#![cfg(test)]

use std::fmt::Debug;

use crate::asm::{
    self as asm, AddrMode, Dotop, SpannedErr, Instr,
    Op, Reg, Size, TrapNum, Value
};

trait CmpOp: Debug {
    fn op_eq(&self, op: &Op) -> bool;
}

impl CmpOp for Dotop {
    fn op_eq(&self, op: &Op) -> bool {
        eprintln!("   left: {:?}", self);
        eprintln!("  right: {:?}", op);
        match op {
            Op::Dotop { op, .. } => op == self,
            _ => false,
        }
    }
}

impl CmpOp for Instr {
    fn op_eq(&self, op: &Op) -> bool {
        eprintln!("   left: {:?}", self);
        eprintln!("  right: {:?}", op);
        match op {
            Op::Instr { instr, .. } => instr == self,
            _ => false,
        }
    }
}

fn op(code: &str, expect: impl CmpOp) {
    let mut code = code.to_string();
    code.push_str("\n.end");

    eprintln!("`{}`", code.escape_debug());
    let rslt = asm::ir(&code).0
        .unwrap();

    let rslt_matches_expected = match &rslt[..] {
        [only] => expect.op_eq(&only.op), // This only happens for Dotop::End
        [first, second] => {
            expect.op_eq(&first.op) &&
            Dotop::End.op_eq(&second.op)
        }
        _ => false,
    };
    assert!(rslt_matches_expected);
}

fn asm(code: &str, expect: &str) -> Result<(), Vec<SpannedErr>> {
    let mut code = code.to_string();
    code.push_str("\n.end");
    let mut expect = expect.to_string();
    expect.push_str(" zz");
    let expect = expect.trim();

    eprintln!("`{}` expecting `{}`", code.escape_debug(), expect);
    let rslt = asm::asm(&code)?;

    if expect == rslt {
        Ok(())
    } else {
        eprintln!("   left: {}", expect);
        eprintln!("  right: {}", rslt);
        Err(vec![])
    }
}

#[test]
fn single_dotops() {
    op(".ADDRSS hello", Dotop::Addrss("hello".to_string()));

    op(".align 2", Dotop::Align(2));
    op(".align 0x4", Dotop::Align(0x4));

    op(".ascii \"hello\"", Dotop::Ascii("hello".to_string()));
    op(".ASCII \"esc_newline\\n\"",
        Dotop::Ascii("esc_newline\n".to_string()));
    op(".ASCII \"hex:\\x20\"", Dotop::Ascii("hex:\x20".to_string()));
    op(".Ascii \"esc quote \\\"\"",
        Dotop::Ascii("esc quote \"".to_string()));

    op(".Block 2", Dotop::Block(2));
    op(".BLOCK 0x04", Dotop::Block(0x04));

    op(".burn 0xffff", Dotop::Burn(0xffff));

    op(".byte 86", Dotop::Byte(86));
    op(".BYTE  0b11110000", Dotop::Byte(0b1111_0000));
    op(".Byte 0o111", Dotop::Byte(0o111));
    op(".byte\t0xfc", Dotop::Byte(0xfc));
    op(".byte 'c'", Dotop::Byte(b'c'));
    op(".byte '\\n'", Dotop::Byte(b'\n'));
    op(".byte '\"'", Dotop::Byte(b'"'));

    op(".end", Dotop::End);
    op(".END", Dotop::End);

    op(".EQUATE 2743", Dotop::Equate(2743));
    op(".equate 0xfc15", Dotop::Equate(0xfc15));
    op(".equate '\\t'", Dotop::Equate(b'\t' as u16));

    op(".word 0", Dotop::Word(0));
    op(".WOrD 0x7000", Dotop::Word(0x7000));
}

#[test]
fn single_instrs() {
    op("SToP", Instr::Stop);
    op("ret", Instr::Ret);
    op("RetTr", Instr::RetTr);
    op("movspa", Instr::MovSPA);
    op("movflga", Instr::MovFlgA);
    op("movaflg", Instr::MovAFlg);

    op("NOTA", Instr::Not(Reg::Acc));
    op("notx", Instr::Not(Reg::Indx));
    op("nega", Instr::Neg(Reg::Acc));
    op("NeGX", Instr::Neg(Reg::Indx));
    op("ASLa", Instr::Asl(Reg::Acc));
    op("asrx", Instr::Asr(Reg::Indx));
    op("roLx", Instr::Rol(Reg::Indx));
    op("ROrA", Instr::Ror(Reg::Acc));

    op("br 5", Instr::Br(AddrMode::Immediate, Value::Num(5)));
    op("Br 0x63", Instr::Br(AddrMode::Immediate, Value::Num(0x63)));
    op("br 0o3,i", Instr::Br(AddrMode::Immediate, Value::Num(0o3)));
    op("bR ')',x", Instr::Br(AddrMode::Indexed, Value::Num(b')' as u16)));
    op("brlt 23  ,  i", Instr::BrLt(AddrMode::Immediate, Value::Num(23)));
    op("BrLe 0x234", Instr::BrLe(AddrMode::Immediate, Value::Num(0x234)));
    op("breq   183", Instr::BrEq(AddrMode::Immediate, Value::Num(183)));
    op("brne 0b0001,\tx", Instr::BrNe(AddrMode::Indexed, Value::Num(1)));
    op("BRGE label",
        Instr::BrGe(AddrMode::Immediate, Value::Label("label".to_string())));
    op("brc spot ,x",
        Instr::BrC(AddrMode::Indexed, Value::Label("spot".to_string())));
    op("BRV 0o73", Instr::BrV(AddrMode::Immediate, Value::Num(0o73)));
    op("call 132", Instr::Call(AddrMode::Immediate, Value::Num(132)));

    op("nop0", Instr::NopU(TrapNum::Zero));
    op("NOp1", Instr::NopU(TrapNum::One));
    op("nop something",
        Instr::Nop(AddrMode::Immediate, Value::Label("something".to_string())));
    op("dEcI 19, n", Instr::Deci(AddrMode::Indirect, Value::Num(19)));
    op("DECO 0 ,s", Instr::Deco(AddrMode::StackRelative, Value::Num(0)));
    op("hexo 81,sf",
        Instr::Hexo(AddrMode::StackRelativeDeferred, Value::Num(81)));
    op("Stro 's',x", Instr::Stro(AddrMode::Indexed, Value::Num(b's' as u16)));
    op("addSP -2,sx",
        Instr::AddSP(AddrMode::StackIndexed, Value::Num((-2i16) as u16)));
    op("SubSP 283,sfx",
        Instr::SubSP(AddrMode::StackDeferredIndexed, Value::Num(283)));

    op("adda -67,i",
        Instr::Add(Reg::Acc, AddrMode::Immediate, Value::Num((-67i16) as u16)));
    op("SUbX 0b1101,n",
        Instr::Sub(Reg::Indx, AddrMode::Indirect, Value::Num(0b1101)));
    op("ANDx 0o557,i",
        Instr::And(Reg::Indx, AddrMode::Immediate, Value::Num(0o557)));
    op("orA 2,d", Instr::Or(Reg::Acc, AddrMode::Direct, Value::Num(2)));

    op("cpba 0,i",
        Instr::Cp(Size::Byte, Reg::Acc, AddrMode::Immediate, Value::Num(0)));
    op("CPWX 9,d",
        Instr::Cp(Size::Word, Reg::Indx, AddrMode::Direct, Value::Num(9)));
    op("LdwA 0,n",
        Instr::Ld(Size::Word, Reg::Acc, AddrMode::Indirect, Value::Num(0)));
    op("lDBx '3',i",
        Instr::Ld(Size::Byte, Reg::Indx, AddrMode::Immediate, Value::Num(b'3' as u16)));
    op("stba 0x12,d",
        Instr::St(Size::Byte, Reg::Acc, AddrMode::Direct, Value::Num(0x12)));
    op("Stwx 0,i",
        Instr::St(Size::Word, Reg::Indx, AddrMode::Immediate, Value::Num(0)));
}

#[test]
fn asm_instrs() {
    let cases = vec![
        ("stop", "00"),
        ("ret", "01"),
        ("rettr", "02"),
        ("movspa", "03"),
        ("movflga", "04"),
        ("movaflg", "05"),

        ("nota", "06"),
        ("notx", "07"),
        ("nega", "08"),
        ("negx", "09"),
        ("asla", "0a"),
        ("aslx", "0b"),
        ("asra", "0c"),
        ("asrx", "0d"),
        ("rola", "0e"),
        ("rolx", "0f"),
        ("rora", "10"),
        ("rorx", "11"),

        ("br 0",     "12 00 00"),
        ("br 0,i",   "12 00 00"),
        ("br 0,x",   "13 00 00"),
        ("brle 0",   "14 00 00"),
        ("brle 0,x", "15 00 00"),
        ("brlt 0",   "16 00 00"),
        ("brlt 0,x", "17 00 00"),
        ("breq 0",   "18 00 00"),
        ("breq 0,x", "19 00 00"),
        ("brne 0,i", "1a 00 00"),
        ("brne 0,x", "1b 00 00"),
        ("brge 0",   "1c 00 00"),
        ("brge 0,x", "1d 00 00"),
        ("brgt 0",   "1e 00 00"),
        ("brgt 0,x", "1f 00 00"),
        ("brv 0",    "20 00 00"),
        ("brv 0,x",  "21 00 00"),
        ("brc 0",    "22 00 00"),
        ("brc 0,x",  "23 00 00"),
        ("call 0,i", "24 00 00"),
        ("call 0,x", "25 00 00"),

        ("nop0", "26"),
        ("nop1", "27"),
        ("nop 0,i", "28 00 00"), //TODO: Should this allow shorthand?

        ("deci 0,d", "31 00 00"),
        ("deci 0,n", "32 00 00"),
        ("deci 0,s", "33 00 00"),
        ("deci 0,sf", "34 00 00"),
        ("deci 0,x", "35 00 00"),
        ("deci 0,sx", "36 00 00"),
        ("deci 0,sfx", "37 00 00"),

        ("deco 0,i", "38 00 00"),
        ("deco 0,d", "39 00 00"),
        ("deco 0,n", "3a 00 00"),
        ("deco 0,s", "3b 00 00"),
        ("deco 0,sf", "3c 00 00"),
        ("deco 0,x", "3d 00 00"),
        ("deco 0,sx", "3e 00 00"),
        ("deco 0,sfx", "3f 00 00"),

        ("hexo 0,i", "40 00 00"),
        ("hexo 0,d", "41 00 00"),
        ("hexo 0,n", "42 00 00"),
        ("hexo 0,s", "43 00 00"),
        ("hexo 0,sf", "44 00 00"),
        ("hexo 0,x", "45 00 00"),
        ("hexo 0,sx", "46 00 00"),
        ("hexo 0,sfx", "47 00 00"),

        ("stro 0,d", "49 00 00"),
        ("stro 0,n", "4a 00 00"),
        ("stro 0,s", "4b 00 00"),
        ("stro 0,sf", "4c 00 00"),
        ("stro 0,x", "4d 00 00"),

        ("addsp 0,i", "50 00 00"),
        ("addsp 0,d", "51 00 00"),
        ("addsp 0,n", "52 00 00"),
        ("addsp 0,s", "53 00 00"),
        ("addsp 0,sf", "54 00 00"),
        ("addsp 0,x", "55 00 00"),
        ("addsp 0,sx", "56 00 00"),
        ("addsp 0,sfx", "57 00 00"),

        ("subsp 0,i", "58 00 00"),
        ("subsp 0,d", "59 00 00"),
        ("subsp 0,n", "5a 00 00"),
        ("subsp 0,s", "5b 00 00"),
        ("subsp 0,sf", "5c 00 00"),
        ("subsp 0,x", "5d 00 00"),
        ("subsp 0,sx", "5e 00 00"),
        ("subsp 0,sfx", "5f 00 00"),

        ("adda 0,i", "60 00 00"),
        ("adda 0,d", "61 00 00"),
        ("adda 0,n", "62 00 00"),
        ("adda 0,s", "63 00 00"),
        ("adda 0,sf", "64 00 00"),
        ("adda 0,x", "65 00 00"),
        ("adda 0,sx", "66 00 00"),
        ("adda 0,sfx", "67 00 00"),
        ("addx 0,i", "68 00 00"),
        ("addx 0,d", "69 00 00"),
        ("addx 0,n", "6a 00 00"),
        ("addx 0,s", "6b 00 00"),
        ("addx 0,sf", "6c 00 00"),
        ("addx 0,x", "6d 00 00"),
        ("addx 0,sx", "6e 00 00"),
        ("addx 0,sfx", "6f 00 00"),

        ("suba 0,i", "70 00 00"),
        ("suba 0,d", "71 00 00"),
        ("suba 0,n", "72 00 00"),
        ("suba 0,s", "73 00 00"),
        ("suba 0,sf", "74 00 00"),
        ("suba 0,x", "75 00 00"),
        ("suba 0,sx", "76 00 00"),
        ("suba 0,sfx", "77 00 00"),
        ("subx 0,i", "78 00 00"),
        ("subx 0,d", "79 00 00"),
        ("subx 0,n", "7a 00 00"),
        ("subx 0,s", "7b 00 00"),
        ("subx 0,sf", "7c 00 00"),
        ("subx 0,x", "7d 00 00"),
        ("subx 0,sx", "7e 00 00"),
        ("subx 0,sfx", "7f 00 00"),

        ("anda 0,i", "80 00 00"),
        ("anda 0,d", "81 00 00"),
        ("anda 0,n", "82 00 00"),
        ("anda 0,s", "83 00 00"),
        ("anda 0,sf", "84 00 00"),
        ("anda 0,x", "85 00 00"),
        ("anda 0,sx", "86 00 00"),
        ("anda 0,sfx", "87 00 00"),
        ("andx 0,i", "88 00 00"),
        ("andx 0,d", "89 00 00"),
        ("andx 0,n", "8a 00 00"),
        ("andx 0,s", "8b 00 00"),
        ("andx 0,sf", "8c 00 00"),
        ("andx 0,x", "8d 00 00"),
        ("andx 0,sx", "8e 00 00"),
        ("andx 0,sfx", "8f 00 00"),

        ("ora 0,i", "90 00 00"),
        ("ora 0,d", "91 00 00"),
        ("ora 0,n", "92 00 00"),
        ("ora 0,s", "93 00 00"),
        ("ora 0,sf", "94 00 00"),
        ("ora 0,x", "95 00 00"),
        ("ora 0,sx", "96 00 00"),
        ("ora 0,sfx", "97 00 00"),
        ("orx 0,i", "98 00 00"),
        ("orx 0,d", "99 00 00"),
        ("orx 0,n", "9a 00 00"),
        ("orx 0,s", "9b 00 00"),
        ("orx 0,sf", "9c 00 00"),
        ("orx 0,x", "9d 00 00"),
        ("orx 0,sx", "9e 00 00"),
        ("orx 0,sfx", "9f 00 00"),

        ("cpwa 0,i", "a0 00 00"),
        ("cpwa 0,d", "a1 00 00"),
        ("cpwa 0,n", "a2 00 00"),
        ("cpwa 0,s", "a3 00 00"),
        ("cpwa 0,sf", "a4 00 00"),
        ("cpwa 0,x", "a5 00 00"),
        ("cpwa 0,sx", "a6 00 00"),
        ("cpwa 0,sfx", "a7 00 00"),
        ("cpwx 0,i", "a8 00 00"),
        ("cpwx 0,d", "a9 00 00"),
        ("cpwx 0,n", "aa 00 00"),
        ("cpwx 0,s", "ab 00 00"),
        ("cpwx 0,sf", "ac 00 00"),
        ("cpwx 0,x", "ad 00 00"),
        ("cpwx 0,sx", "ae 00 00"),
        ("cpwx 0,sfx", "af 00 00"),

        ("cpba 0,i", "b0 00 00"),
        ("cpba 0,d", "b1 00 00"),
        ("cpba 0,n", "b2 00 00"),
        ("cpba 0,s", "b3 00 00"),
        ("cpba 0,sf", "b4 00 00"),
        ("cpba 0,x", "b5 00 00"),
        ("cpba 0,sx", "b6 00 00"),
        ("cpba 0,sfx", "b7 00 00"),
        ("cpbx 0,i", "b8 00 00"),
        ("cpbx 0,d", "b9 00 00"),
        ("cpbx 0,n", "ba 00 00"),
        ("cpbx 0,s", "bb 00 00"),
        ("cpbx 0,sf", "bc 00 00"),
        ("cpbx 0,x", "bd 00 00"),
        ("cpbx 0,sx", "be 00 00"),
        ("cpbx 0,sfx", "bf 00 00"),

        ("ldwa 0,i", "c0 00 00"),
        ("ldwa 0,d", "c1 00 00"),
        ("ldwa 0,n", "c2 00 00"),
        ("ldwa 0,s", "c3 00 00"),
        ("ldwa 0,sf", "c4 00 00"),
        ("ldwa 0,x", "c5 00 00"),
        ("ldwa 0,sx", "c6 00 00"),
        ("ldwa 0,sfx", "c7 00 00"),
        ("ldwx 0,i", "c8 00 00"),
        ("ldwx 0,d", "c9 00 00"),
        ("ldwx 0,n", "ca 00 00"),
        ("ldwx 0,s", "cb 00 00"),
        ("ldwx 0,sf", "cc 00 00"),
        ("ldwx 0,x", "cd 00 00"),
        ("ldwx 0,sx", "ce 00 00"),
        ("ldwx 0,sfx", "cf 00 00"),

        ("ldba 0,i", "d0 00 00"),
        ("ldba 0,d", "d1 00 00"),
        ("ldba 0,n", "d2 00 00"),
        ("ldba 0,s", "d3 00 00"),
        ("ldba 0,sf", "d4 00 00"),
        ("ldba 0,x", "d5 00 00"),
        ("ldba 0,sx", "d6 00 00"),
        ("ldba 0,sfx", "d7 00 00"),
        ("ldbx 0,i", "d8 00 00"),
        ("ldbx 0,d", "d9 00 00"),
        ("ldbx 0,n", "da 00 00"),
        ("ldbx 0,s", "db 00 00"),
        ("ldbx 0,sf", "dc 00 00"),
        ("ldbx 0,x", "dd 00 00"),
        ("ldbx 0,sx", "de 00 00"),
        ("ldbx 0,sfx", "df 00 00"),

        ("stwa 0,d", "e1 00 00"),
        ("stwa 0,n", "e2 00 00"),
        ("stwa 0,s", "e3 00 00"),
        ("stwa 0,sf", "e4 00 00"),
        ("stwa 0,x", "e5 00 00"),
        ("stwa 0,sx", "e6 00 00"),
        ("stwa 0,sfx", "e7 00 00"),
        ("stwx 0,d", "e9 00 00"),
        ("stwx 0,n", "ea 00 00"),
        ("stwx 0,s", "eb 00 00"),
        ("stwx 0,sf", "ec 00 00"),
        ("stwx 0,x", "ed 00 00"),
        ("stwx 0,sx", "ee 00 00"),
        ("stwx 0,sfx", "ef 00 00"),

        ("stba 0,d", "f1 00 00"),
        ("stba 0,n", "f2 00 00"),
        ("stba 0,s", "f3 00 00"),
        ("stba 0,sf", "f4 00 00"),
        ("stba 0,x", "f5 00 00"),
        ("stba 0,sx", "f6 00 00"),
        ("stba 0,sfx", "f7 00 00"),
        ("stbx 0,d", "f9 00 00"),
        ("stbx 0,n", "fa 00 00"),
        ("stbx 0,s", "fb 00 00"),
        ("stbx 0,sf", "fc 00 00"),
        ("stbx 0,x", "fd 00 00"),
        ("stbx 0,sx", "fe 00 00"),
        ("stbx 0,sfx", "ff 00 00"),
    ];

    for (code, rslt) in cases {
        asm(code, rslt).unwrap();
    }
}

#[test]
fn fail_asm_instrs() {
    let cases = vec![
        "br 0,d",
        "br 0,n",
        "br 0,s",
        "br 0,sf",
        "br 0,sx",
        "br 0,sfx",

        "brle 0,d",
        "brle 0,n",
        "brle 0,s",
        "brle 0,sf",
        "brle 0,sx",
        "brle 0,sfx",

        "brlt 0,d",
        "brlt 0,n",
        "brlt 0,s",
        "brlt 0,sf",
        "brlt 0,sx",
        "brlt 0,sfx",

        "breq 0,d",
        "breq 0,n",
        "breq 0,s",
        "breq 0,sf",
        "breq 0,sx",
        "breq 0,sfx",

        "brne 0,d",
        "brne 0,n",
        "brne 0,s",
        "brne 0,sf",
        "brne 0,sx",
        "brne 0,sfx",

        "brge 0,d",
        "brge 0,n",
        "brge 0,s",
        "brge 0,sf",
        "brge 0,sx",
        "brge 0,sfx",

        "brgt 0,d",
        "brgt 0,n",
        "brgt 0,s",
        "brgt 0,sf",
        "brgt 0,sx",
        "brgt 0,sfx",

        "brv 0,d",
        "brv 0,n",
        "brv 0,s",
        "brv 0,sf",
        "brv 0,sx",
        "brv 0,sfx",

        "brc 0,d",
        "brc 0,n",
        "brc 0,s",
        "brc 0,sf",
        "brc 0,sx",
        "brc 0,sfx",

        "nop 0,d",
        "nop 0,n",
        "nop 0,s",
        "nop 0,sf",
        "nop 0,sx",
        "nop 0,sfx",

        "deci 0,i",
        "stro 0,i",
        "stro 0,sx",
        "stro 0,sfx",
        "stwa 0,i",
        "stwx 0,i",
        "stba 0,i",
        "stbx 0,i",
    ];

    for case in cases {
        assert!(asm(case, "").is_err());
    }
}

const DOTOPS: &str = r###"
.ADDRSS hello
  .addrss hello
.addRSS hEllo


  .ALIGN 82

.align    0x6234

   .AliGn 0b0111011
.align  0o770

.ascii "\x00"
.end
"###;

#[test]
fn dotops() {
    dbg!(&DOTOPS[60..70]);

    let lines = asm::ir(DOTOPS).0
        .unwrap();

    let dotops = vec![
        Dotop::Addrss("hello".to_string()),
        Dotop::Addrss("hello".to_string()),
        Dotop::Addrss("hEllo".to_string()),
        Dotop::Align(82),
        Dotop::Align(0x6234),
        Dotop::Align(0b011_1011),
        Dotop::Align(0o770),
        Dotop::Ascii("\0".to_string()),
    ];

    for (expect, line) in dotops.iter().zip(lines.iter()) {
        assert!(expect.op_eq(&line.op));
    }
}

#[test]
fn ascii() {
    let cases = vec![
        (".align 8", "00 00 00 00 00 00 00 00"),
        (r#".ascii "text""#, "74 65 78 74"),
        (r#".ASCII "\x00""#, "00"),
        (r#".Ascii "\"text\"""#, "22 74 65 78 74 22"),
        (r#".ascii "\"""#, "22"),
        (r#".ascii "\\""#, "5c"),
        (r#".ascii "\n""#, "0a"),

        (".block 3", "00 00 00"),
        (".byte '\\n'", "0a"),

        ("lab: .equate 10", ""),
        (".word 0x3842", "38 42"),
    ];

    for (code, rslt) in cases {
        asm(code, rslt).unwrap();
    }
}

#[test]
fn labels() {
    let cases = vec![
        (".word 0", vec![]),
        ("lab1: .word 1", vec!["lab1"]),
        ("eq: .equate 10", vec!["eq"]),
        ("main: deci 82,d", vec!["main"]),
        ("UPPER: rora", vec!["UPPER"]),

        ("lab1: lab2: .byte 1", vec!["lab1", "lab2"]),
        ("main:\ndeci 81,s", vec!["main"]),
    ];

    for (code, labels) in cases {
        eprintln!("`{}`", code.escape_debug());

        let lines = dbg!(asm::ir(&code).0.unwrap());
        match &lines[..] {
            [op] => {
                eprintln!("  left: {:?}", labels);
                eprintln!(" right: {:?}", op.labels);
                assert_eq!(labels, op.labels);
            }
            _ => panic!("unexpected number of lines"),
        }
    }
}
