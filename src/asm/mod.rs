#[cfg(test)]
mod test;
mod cmp;
mod tokens;

pub use cmp::obj_eq;

use std::collections::{HashMap, HashSet};
use std::convert::TryFrom;
use std::fmt::Display;
use std::iter;
use std::ops::Range;

use ariadne::{Color, Fmt, Label, Report, ReportKind};
use chumsky::error::SimpleReason;
use chumsky::prelude::*;

use tokens::{Token, TokenType};

#[derive(Debug)]
pub enum Error {
    AddrMode(Instr),

    Alignment(u16),

    AsciiByteTooLong,

    ExpectedFound {
        expected: Vec<Option<TokenType>>,
        found: Option<TokenType>,
    },

    MissingEnd,

    NumByteTooBig,

    Token(Simple<char>),

    UnknownSymbol(String),
}

impl Error {
    fn msg(&self) -> String {
        match self {
            Error::AddrMode(instr) =>
                format!("invalid address mode `{}`", instr.mode_spec()),
            Error::Alignment(amnt) =>
                format!("invalid alignment `{}`", amnt),
            Error::AsciiByteTooLong | Error::NumByteTooBig =>
                format!("literal too big"),
            Error::ExpectedFound { found: Some(found), .. } =>
                format!("unexpected {}", found.typ_name()),
            Error::ExpectedFound { found: None, .. } =>
                format!("unexpected EOF"),
            Error::MissingEnd =>
                format!("missing `.END` pseudo-op"),
            Error::Token(err) => if let Some(found) = err.found() {
                format!("unexpected `{}`", found)
            } else {
                format!("unexpected eof")
            }
            Error::UnknownSymbol(symbol) =>
                format!("undefined symbol `{}`", symbol),
        }
    }

    fn label(&self) -> String {
        match self {
            Error::AddrMode(instr) =>
                format!(
                    "invalid address mode {} for {}",
                    instr.mode_spec().fg(Color::Default),
                    instr.mnemonic().fg(Color::Blue),
                ),
            Error::Alignment(amnt) =>
                format!("invalid alignment amount {}", amnt),
            Error::AsciiByteTooLong =>
                format!("this string is too long"),
            Error::ExpectedFound { expected, .. } =>
                expected_report(expected.iter()),
            Error::MissingEnd =>
                format!("expected `.END` here"),
            Error::NumByteTooBig =>
                format!("this number is too large"),
            Error::Token(err) =>
                expected_report(err.expected()),
            Error::UnknownSymbol(symbol) =>
                format!(
                    "symbol `{}` is undefined",
                    symbol.fg(Color::Magenta),
                ),
        }
    }

    fn note(&self) -> String {
        match self {
            Error::AddrMode(instr) =>
                format!(
                    "valid address modes for {} are {}",
                    instr.mnemonic().fg(Color::Blue),
                    instr.valid_addrmodes()
                        .iter()
                        .map(|mode| mode.spec() )
                        .collect::<Vec<_>>()
                        .join(","),
                ),
            Error::Alignment(_) =>
                format!("valid .ALIGN values are 2, 4, 8"),
            Error::AsciiByteTooLong =>
                format!("string byte literals must be one character long"),
            Error::ExpectedFound { .. } =>
                format!(""),
            Error::MissingEnd =>
                format!("try adding `.END`"),
            Error::NumByteTooBig =>
                format!("numeric byte literals must be less than 0xff"),
            Error::Token(err) => match err.reason() {
                SimpleReason::Unexpected => format!(""),
                SimpleReason::Unclosed { delimiter, .. } =>
                    format!("try inserting a {}", delimiter),
                SimpleReason::Custom(msg) => msg.clone(),
            }
            Error::UnknownSymbol(symbol) =>
                format!(
                    "define {} on another line before using it",
                    symbol.fg(Color::Magenta),
                ),
        }
    }
}

fn expected_report<'a>(
    expected: impl Iterator<Item = &'a Option<impl Display + 'a>>,
) -> String {
    let mut report = if let (_, Some(hint)) = expected.size_hint() {
        Vec::with_capacity(hint)
    } else {
        vec![]
    };

    for maybe_toktyp in expected {
        if let Some(toktyp) = maybe_toktyp {
            let ident = toktyp.to_string();
            if ! report.contains(&ident) {
                report.push(ident)
            }
        } else {
            report.push("end of input".to_string());
        }
    }
    let report = match &report[..] {
        [] => "end of input".to_string(),
        [only] => only.to_string(),
        [others @ .., last] =>
            format!("{}, or {}", others.join(", "), last)
    };
    format!("expected {}", report)
}

#[derive(Debug)]
pub struct SpannedErr {
    err: Error,
    span: Range<usize>,
}

impl SpannedErr {
    fn new(span: Range<usize>, err: Error) -> SpannedErr {
        SpannedErr {
            err,
            span,
        }
    }

    fn expected_found(
        span: Range<usize>,
        expected: TokenType,
        found: TokenType
    ) -> SpannedErr {
        SpannedErr {
            err: Error::ExpectedFound {
                expected: vec![Some(expected)],
                found: Some(found),
            },
            span,
        }
    }

    pub fn report<'a>(
        &'a self,
        filename: &'a str,
    ) -> Report<(&str, Range<usize>)> {
        let mut rpt = Report::build(ReportKind::Error, filename, self.span.start)
            .with_message(self.err.msg())
            .with_label(
                Label::new((filename, self.span.clone()))
                    .with_message(self.err.label())
                    .with_color(Color::Red)
            );
        let note = self.err.note();
        if note.len() != 0 {
            rpt = rpt.with_note(self.err.note());
        }
        rpt.finish()
    }
}

impl chumsky::Error<Token> for SpannedErr {
    type Span = Range<usize>;
    type Label = &'static str;

    fn expected_input_found<I: IntoIterator<Item = Option<Token>>>(
        _span: Range<usize>,
        expected: I,
        found: Option<Token>,
    ) -> Self {
        let expected = expected.into_iter()
            .map(|maybe_tok| maybe_tok.map(|tok| tok.typ ) )
            .collect();
        if let Some(found) = found {
            SpannedErr {
                err: Error::ExpectedFound {
                    expected,
                    found: Some(found.typ),
                },
                span: found.span,
            }
        } else {
            SpannedErr {
                err: Error::ExpectedFound {
                    expected,
                    found: None,
                },
                span: 0..0 //TODO: What to do here?
            }
        }
    }

    fn with_label(self, _label: Self::Label) -> Self {
        self
    }

    fn merge(mut self, mut other: Self) -> Self {
        match (&mut self.err, &mut other.err) {
            (
                Error::ExpectedFound { expected, .. },
                Error::ExpectedFound { expected: other_expected, .. },
            ) => expected.append(other_expected),
            _ => {}
        }
        self
    }
}

impl From<Simple<char>> for SpannedErr {
    fn from(err: Simple<char>) -> SpannedErr {
        let span = err.span().clone();
        SpannedErr {
            err: Error::Token(err),
            span,
        }
    }
}

trait Obj {
    fn obj(&self) -> String;
}

impl Obj for u8 {
    fn obj(&self) -> String {
        format!("{:02x}", self)
    }
}

impl Obj for u16 {
    fn obj(&self) -> String {
        let [b0, b1] = self.to_be_bytes();
        format!("{:02x} {:02x}", b0, b1)
    }
}

fn align_len(pos: u16, to: u16) -> u16 {
    if pos >= to {
        0
    } else {
        to - pos
    }
}

fn block(size: u16) -> String {
    let mut rslt = String::with_capacity(size as usize * 3);
    for _ in 0..size {
        rslt.push_str("00 ");
    }
    rslt.pop();
    rslt
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Dotop {
    Addrss(String),
    Align(u16),
    Ascii(String),
    Block(u16),
    Burn(u16),
    Byte(u8),
    End,
    Equate(u16),
    Word(u16),
}

impl Dotop {
    fn size(&self, pos: u16) -> u16 {
        match self {
            Dotop::Addrss(_) => 2,
            Dotop::Align(to) => align_len(pos, *to),
            Dotop::Ascii(s) => s.len() as u16,
            Dotop::Block(size) => *size,
            Dotop::Burn(_) => 0,
            Dotop::Byte(_) => 1,
            Dotop::End => 0,
            Dotop::Equate(_) => 0,
            Dotop::Word(_) => 2,
        }
    }

    fn check_arg(&self) -> Result<(), Error> {
        match self {
            Dotop::Align(on) => match *on {
                2 | 4 | 8 => Ok(()),
                _ => Err(Error::Alignment(*on)),
            }
            _ => Ok(()),
        }
    }

    fn obj(
        &self,
        pos: u16,
        symbols: &HashMap<&str, u16>,
    ) -> Result<String, Error> {
        Ok(match self {
            Dotop::Addrss(sym) => symbols.get(sym.as_str())
                .ok_or(Error::UnknownSymbol(sym.clone()))?.obj(),
            Dotop::Align(to) => block(align_len(pos, *to)),
            Dotop::Ascii(s) => {
                let mut rslt = String::with_capacity(s.len() * 3);
                for byte in s.as_bytes() {
                    rslt.push_str(&byte.obj());
                    rslt.push(' ');
                }
                rslt.pop();
                rslt
            }
            Dotop::Block(size) => block(*size),
            Dotop::Burn(_) => {
                eprintln!("warn: .BURN unimplemented");
                String::new()
            }
            Dotop::Byte(b) => b.obj(),
            Dotop::End |
            Dotop::Equate(_) => String::new(),
            Dotop::Word(w) => w.obj(),
        })
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
#[repr(u8)]
pub enum Reg {
    Acc = 0,
    Indx = 1,
}

impl Reg {
    fn left_bits(self) -> u8 {
        (self as u8) << 3
    }

    fn spec(self) -> &'static str {
        match self {
            Reg::Acc => "A",
            Reg::Indx => "X",
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Value {
    Num(u16),
    Label(String),
}

impl Value {
    pub fn obj(&self, symbols: &HashMap<&str, u16>) -> Result<String, Error> {
        Ok(match self {
            Value::Num(w) => w.obj(),
            Value::Label(sym) => symbols.get(sym.as_str())
                .ok_or(Error::UnknownSymbol(sym.clone()))?.obj(),
        })
    }
}

impl From<u16> for Value {
    fn from(num: u16) -> Value {
        Value::Num(num)
    }
}

impl From<String> for Value {
    fn from(s: String) -> Value {
        Value::Label(s)
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
#[repr(u8)]
pub enum TrapNum {
    Zero = 0x00,
    One  = 0x01,
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
#[repr(u8)]
pub enum AddrMode {
    Immediate             = 0b000,
    Direct                = 0b001,
    Indirect              = 0b010,
    StackRelative         = 0b011,
    StackRelativeDeferred = 0b100,
    Indexed               = 0b101,
    StackIndexed          = 0b110,
    StackDeferredIndexed  = 0b111,
}

impl AddrMode {
    fn spec(&self) -> &'static str {
        match self {
            AddrMode::Immediate => "i",
            AddrMode::Direct => "d",
            AddrMode::Indirect => "n",
            AddrMode::StackRelative => "s",
            AddrMode::StackRelativeDeferred => "sf",
            AddrMode::Indexed => "x",
            AddrMode::StackIndexed => "sx",
            AddrMode::StackDeferredIndexed => "sfx",
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
#[repr(u8)]
pub enum Size {
    Word = 0x00,
    Byte = 0x10,
}

impl Size {
    pub fn spec(self) -> &'static str {
        match self {
            Size::Word => "W",
            Size::Byte => "B",
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
#[repr(u8)]
pub enum Instr {
    Stop    = 0x00,
    Ret     = 0x01,
    RetTr   = 0x02,
    MovSPA  = 0x03,
    MovFlgA = 0x04,
    MovAFlg = 0x05,

    Not(Reg) = 0x06,
    Neg(Reg) = 0x08,
    Asl(Reg) = 0x0a,
    Asr(Reg) = 0x0c,
    Rol(Reg) = 0x0e,
    Ror(Reg) = 0x10,

    Br(AddrMode, Value)   = 0x12,
    BrLe(AddrMode, Value) = 0x14,
    BrLt(AddrMode, Value) = 0x16,
    BrEq(AddrMode, Value) = 0x18,
    BrNe(AddrMode, Value) = 0x1a,
    BrGe(AddrMode, Value) = 0x1c,
    BrGt(AddrMode, Value) = 0x1e,
    BrV(AddrMode, Value)  = 0x20,
    BrC(AddrMode, Value)  = 0x22,
    Call(AddrMode, Value) = 0x24,

    NopU(TrapNum)         = 0x26,
    Nop(AddrMode, Value)  = 0x28,
    Deci(AddrMode, Value) = 0x30,
    Deco(AddrMode, Value) = 0x38,
    Hexo(AddrMode, Value) = 0x40,
    Stro(AddrMode, Value) = 0x48,

    AddSP(AddrMode, Value) = 0x50,
    SubSP(AddrMode, Value) = 0x58,

    Add(Reg, AddrMode, Value) = 0x60,
    Sub(Reg, AddrMode, Value) = 0x70,
    And(Reg, AddrMode, Value) = 0x80,
    Or(Reg, AddrMode, Value)  = 0x90,

    Cp(Size, Reg, AddrMode, Value) = 0xa0,
    Ld(Size, Reg, AddrMode, Value) = 0xc0,
    St(Size, Reg, AddrMode, Value) = 0xe0,
}

impl Instr {
    pub fn mnemonic(&self) -> String {
        let mut simple = match self {
            Instr::Stop => "STOP",
            Instr::Ret => "RET",
            Instr::RetTr => "RETTR",
            Instr::MovSPA => "MOVSPA",
            Instr::MovFlgA => "MOVFLGA",
            Instr::MovAFlg => "MOVAFLG",
            Instr::Br(..) => "BR",
            Instr::BrLe(..) => "BRLE",
            Instr::BrLt(..) => "BRLT",
            Instr::BrEq(..) => "BREQ",
            Instr::BrNe(..) => "BRNE",
            Instr::BrGe(..) => "BRGE",
            Instr::BrGt(..) => "BRGT",
            Instr::BrV(..) => "BRV",
            Instr::BrC(..) => "BRC",
            Instr::Call(..) => "CALL",

            Instr::Nop(..) | Instr::NopU(..)  => "NOP",
            Instr::Deci(..) => "DECI",
            Instr::Deco(..) => "DECO",
            Instr::Hexo(..) => "HEXO",
            Instr::Stro(..) => "STRO",
            Instr::AddSP(..) => "ADDSP",
            Instr::SubSP(..) => "SUBSP",

            Instr::Not(..) => "NOT",
            Instr::Neg(..) => "NEG",
            Instr::Asl(..) => "ASL",
            Instr::Asr(..) => "ASR",
            Instr::Rol(..) => "ROL",
            Instr::Ror(..) => "ROR",
            Instr::Add(..) => "ADD",
            Instr::Sub(..) => "SUB",
            Instr::And(..) => "AND",
            Instr::Or(..) => "OR",
            Instr::Cp(..) => "CP",
            Instr::Ld(..) => "LD",
            Instr::St(..) => "ST",
        }.to_string();

        let post = match self {
            Instr::NopU(num) => format!("{}", *num as u8),

            Instr::Not(reg) | Instr::Neg(reg) | Instr::Asl(reg) |
            Instr::Asr(reg) | Instr::Rol(reg) | Instr::Ror(reg) |
            Instr::Add(reg, _, _) | Instr::Sub(reg, _, _) |
            Instr::And(reg, _, _) | Instr::Or(reg, _, _) => {
                reg.spec().to_string()
            }

            Instr::Cp(size, reg, _, _) |
            Instr::Ld(size, reg, _, _) |
            Instr::St(size, reg, _, _) => {
                format!("{}{}", size.spec(), reg.spec())
            }
            _ => String::new(),
        };

        simple.push_str(&post);
        simple
    }

    pub fn mode_spec(&self) -> &'static str {
        match self {
            Instr::Br(mode, _) | Instr::Call(mode, _) |
            Instr::BrLt(mode, _) | Instr::BrLe(mode, _) |
            Instr::BrEq(mode, _) | Instr::BrNe(mode, _) |
            Instr::BrGe(mode, _) | Instr::BrGt(mode, _) |
            Instr::BrC(mode, _) | Instr::BrV(mode, _) |
            Instr::Nop(mode, _) |
            Instr::Deci(mode, _) | Instr::Deco(mode, _) |
            Instr::Hexo(mode, _) | Instr::Stro(mode, _) |
            Instr::AddSP(mode, _) | Instr::SubSP(mode, _) |
            Instr::Add(_, mode, _) | Instr::Sub(_, mode, _) |
            Instr::And(_, mode, _) | Instr::Or(_, mode, _) |
            Instr::Cp(_, _, mode, _) |
            Instr::Ld(_, _, mode, _) |
            Instr::St(_, _, mode, _) => {
                mode.spec()
            }
            _ => "U",
        }
    }

    pub fn valid_addrmodes(&self) -> &'static [AddrMode] {
        if self.size() == 1 {
            &[]
        } else {
            match self {
                Instr::Br(_, _) | Instr::Call(_, _) |
                Instr::BrLt(_, _) | Instr::BrLe(_, _) |
                Instr::BrEq(_, _) | Instr::BrNe(_, _) |
                Instr::BrGe(_, _) | Instr::BrGt(_, _) |
                Instr::BrC(_, _) | Instr::BrV(_, _) => {
                    &[AddrMode::Immediate, AddrMode::Indexed]
                }
                Instr::Nop(..) => &[AddrMode::Immediate],
                Instr::Deci(..) | Instr::St(..) => &[
                    AddrMode::Direct,
                    AddrMode::Indirect,
                    AddrMode::StackRelative,
                    AddrMode::StackRelativeDeferred,
                    AddrMode::Indexed,
                    AddrMode::StackIndexed,
                    AddrMode::StackDeferredIndexed,
                ],
                Instr::Stro(..) => &[
                    AddrMode::Direct,
                    AddrMode::Indirect,
                    AddrMode::StackRelative,
                    AddrMode::StackRelativeDeferred,
                    AddrMode::Indexed,
                ],
                _ => &[
                    AddrMode::Immediate,
                    AddrMode::Direct,
                    AddrMode::Indirect,
                    AddrMode::StackRelative,
                    AddrMode::StackRelativeDeferred,
                    AddrMode::Indexed,
                    AddrMode::StackIndexed,
                    AddrMode::StackDeferredIndexed,
                ],
            }
        }
    }

    pub fn size(&self) -> u16 {
        match self {
            Instr::Stop | Instr::Ret | Instr::RetTr |
            Instr::MovSPA | Instr::MovAFlg | Instr::MovFlgA |
            Instr::Not(_) | Instr::Neg(_) | Instr::Asl(_) | Instr::Asr(_) |
            Instr::Rol(_) | Instr::Ror(_) | Instr::NopU(_) => 1,
            _ => 3,
        }
    }

    // Alias for std::intrinsics::discriminant_value
    fn bits(&self) -> u8 {
        std::intrinsics::discriminant_value(self)
    }

    pub fn obj(&self, symbols: &HashMap<&str, u16>) -> Result<String, Error> {
       Ok(match self {
            Instr::Stop | Instr::Ret | Instr::RetTr |
            Instr::MovSPA | Instr::MovAFlg | Instr::MovFlgA => {
                self.bits().obj()
            }
            Instr::Not(reg) | Instr::Neg(reg) | Instr::Asl(reg) |
            Instr::Asr(reg) | Instr::Rol(reg) | Instr::Ror(reg) => {
                (self.bits() | (*reg as u8)).obj()
            }
            Instr::Br(mode, val) | Instr::Call(mode, val) |
            Instr::BrLt(mode, val) | Instr::BrLe(mode, val) |
            Instr::BrEq(mode, val) | Instr::BrNe(mode, val) |
            Instr::BrGe(mode, val) | Instr::BrGt(mode, val) |
            Instr::BrC(mode, val) | Instr::BrV(mode, val) => {
                let mut obj = (self.bits() | ((*mode as u8) >> 2)).obj();
                obj.push(' ');
                obj.push_str(&val.obj(symbols)?);
                obj
            }
            Instr::Nop(mode, val) |
            Instr::Deci(mode, val) | Instr::Deco(mode, val) |
            Instr::Hexo(mode, val) | Instr::Stro(mode, val) |
            Instr::AddSP(mode, val) | Instr::SubSP(mode, val) => {
                let mut obj = (self.bits() | (*mode as u8)).obj();
                obj.push(' ');
                obj.push_str(&val.obj(symbols)?);
                obj
            }
            Instr::NopU(num) => (self.bits() | (*num as u8)).obj(),
            Instr::Add(reg, mode, val) | Instr::Sub(reg, mode, val) |
            Instr::And(reg, mode, val) | Instr::Or(reg, mode, val) => {
                let mut obj = (self.bits()
                    | reg.left_bits()
                    | (*mode as u8)).obj();
                obj.push(' ');
                obj.push_str(&val.obj(symbols)?);
                obj
            }
            Instr::Cp(size, reg, mode, val) |
            Instr::Ld(size, reg, mode, val) |
            Instr::St(size, reg, mode, val) => {
                let mut obj = (self.bits()
                    | (*size as u8)
                    | reg.left_bits()
                    | (*mode as u8)).obj();
                obj.push(' ');
                obj.push_str(&val.obj(symbols)?);
                obj
            }
        })
    }

    fn check_addrmode(&self) -> Result<(), Error> {
        match self {
            Instr::Br(mode, _) | Instr::Call(mode, _) |
            Instr::BrLt(mode, _) | Instr::BrLe(mode, _) |
            Instr::BrEq(mode, _) | Instr::BrNe(mode, _) |
            Instr::BrGt(mode, _) | Instr::BrGe(mode, _) |
            Instr::BrC(mode, _) | Instr::BrV(mode, _) => {
                match mode {
                    AddrMode::Immediate | AddrMode::Indexed => Ok(()),
                    _ => Err(Error::AddrMode(self.clone())),
                }
            }
            Instr::Nop(mode, _) => match mode {
                AddrMode::Immediate => Ok(()),
                _ => Err(Error::AddrMode(self.clone())),
            }
            Instr::Deci(mode, _) => match mode {
                AddrMode::Immediate => Err(Error::AddrMode(self.clone())),
                _ => Ok(()),
            }
            Instr::Stro(mode, _) => match mode {
                AddrMode::Direct | AddrMode::Indirect |
                AddrMode::StackRelative | AddrMode::StackRelativeDeferred |
                AddrMode::Indexed => Ok(()),
                _ => Err(Error::AddrMode(self.clone())),
            }
            Instr::St(_, _, mode, _) => match mode {
                AddrMode::Immediate => Err(Error::AddrMode(self.clone())),
                _ => Ok(()),
            }
            _ => Ok(()),
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum Op {
    Dotop {
        op: Dotop,
        op_span: Range<usize>,
        arg_span: Option<Range<usize>>,
    },
    Instr {
        instr: Instr,
        op_span: Range<usize>,
        // (arg, mode)
        arg_span: Option<(Range<usize>, Range<usize>)>
    },
}

impl Op {
    pub fn size(&self, pos: u16) -> u16 {
        match self {
            Op::Dotop { op, .. } => op.size(pos),
            Op::Instr { instr, .. } => instr.size(),
        }
    }

    pub fn obj(
        &self,
        pos: u16,
        symbols: &HashMap<&str, u16>,
    ) -> Result<String, Error> {
        match self {
            Op::Dotop { op, .. } => op.obj(pos, symbols),
            Op::Instr { instr, .. } => instr.obj(symbols),
        }
    }
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Line {
    pub labels: Vec<String>,
    pub op: Op,
}

fn space() -> impl Parser<Token, (), Error = SpannedErr> {
    filter_map(|_, tok: Token| match tok.typ {
        TokenType::Space => Ok(()),
        ty => Err(SpannedErr::expected_found(tok.span, TokenType::Space, ty)),
    })
}

fn comma() -> impl Parser<Token, (), Error = SpannedErr> {
    filter_map(|_, tok: Token| match tok.typ {
        TokenType::Comma => Ok(()),
        ty => Err(SpannedErr::expected_found(tok.span, TokenType::Comma, ty)),
    })
}

fn comment() -> impl Parser<Token, (), Error = SpannedErr> {
    filter_map(|_, tok: Token| match tok.typ {
        TokenType::Comment(_) => Ok(()),
        ty => Err(SpannedErr::expected_found(
            tok.span, TokenType::Comment("".to_string()), ty
        )),
    })
}

fn ascii() -> impl Parser<Token, (String, Token), Error = SpannedErr> {
    filter_map(|_, tok: Token| match tok.typ {
        TokenType::Str(ref s) => Ok((s.clone(), tok)),
        ty => Err(SpannedErr::expected_found(
            tok.span, TokenType::Str("".to_string()), ty
        )),
    })
}

fn char_lit() -> impl Parser<Token, (u8, Token), Error = SpannedErr> {
    filter_map(|_, tok: Token| match tok.typ {
        TokenType::Char(c) => Ok((c, tok)),
        ty => Err(SpannedErr::expected_found(
            tok.span, TokenType::Char(b'\0'), ty
        )),
    })
}

fn caseless_keyword(expect: &str)
    -> impl Parser<Token, Token, Error = SpannedErr> + '_
{
    filter_map(move |_, tok: Token| match tok.typ {
        TokenType::Ident(ref found) if found.eq_ignore_ascii_case(expect) =>
            Ok(tok),
        ty => Err(SpannedErr::expected_found(
            tok.span, TokenType::Ident(expect.to_string()), ty
        )),
    })
}

fn ident() -> impl Parser<Token, (String, Token), Error = SpannedErr> {
    filter_map(|_, tok: Token| match tok.typ {
        TokenType::Ident(ref s) => Ok((s.clone(), tok)),
        ty => Err(SpannedErr::expected_found(
            tok.span, TokenType::Ident("".to_string()), ty
        )),
    })
}

fn newline() -> impl Parser<Token, (), Error = SpannedErr> {
    filter_map(|_, tok: Token| match tok.typ {
        TokenType::Newline => Ok(()),
        ty => Err(SpannedErr::expected_found(
            tok.span, TokenType::Newline, ty
        )),
    })
}

fn number() -> impl Parser<Token, (u16, Token), Error = SpannedErr> {
    filter_map(|_, tok: Token| match tok.typ {
        TokenType::Num(n) => Ok((n, tok)),
        ty => Err(SpannedErr::expected_found(
            tok.span, TokenType::Num(0), ty
        )),
    })
}

fn literal() -> impl Parser<Token, (u16, Token), Error = SpannedErr> {
    choice((
        char_lit()
            .map(|(byte, tok)| (byte as u16, tok) ),
        number(),
        //TODO: 2-byte ascii string literals
    ))
}

fn value() -> impl Parser<Token, (Value, Token), Error = SpannedErr> {
    choice((
        ident().map(|(s, tok)| (Value::from(s), tok) ),
        literal().map(|(n, tok)| (Value::from(n), tok) ),
    ))
}

fn whitespace() -> impl Parser<Token, (), Error = SpannedErr> {
    choice((
        space(),
        newline(),
        comment(),
    ))
        .repeated()
        .ignored()
}

fn dotop<AP, A, R, C>(
    op: tokens::Dotop,
    arg_parser: AP,
    construct: C,
) -> impl Parser<Token, (R, Token, Token), Error = SpannedErr>
where
    AP: Parser<Token, (A, Token), Error = SpannedErr>,
    C: Fn(A) -> R + 'static,
{
    filter_map(move |_, tok: Token| match tok.typ {
        TokenType::Dotop(found_op) if op == found_op =>
            Ok(tok),
        ty => Err(SpannedErr::expected_found(
            tok.span, TokenType::Dotop(op), ty
        )),
    })
        .then_ignore(space())
        .then(arg_parser)
        .map(move |(op_tok, (arg, arg_tok))|
            (construct(arg), op_tok, arg_tok)
        )
}

fn reg() -> impl Parser<char, Reg, Error = Simple<char>> {
    choice((
        choice((just('a'), just('A'))).to(Reg::Acc),
        choice((just('x'), just('X'))).to(Reg::Indx),
    ))
}

fn just_ignore_case(word: &'static str)
    -> impl Parser<char, String, Error = Simple<char>>
{
    filter(move |c: &char|
        word.contains(c.to_ascii_uppercase()) ||
        word.contains(c.to_ascii_lowercase())
    )
        .repeated()
        .exactly(word.len())
        .collect()
        .try_map(move |s: String, span: Range<usize>| {
            if s.eq_ignore_ascii_case(word) {
                Ok(s)
            } else {
                Err(Simple::expected_input_found(
                    span.clone(),
                    iter::once(word[span.clone()].chars().next()),
                    s[span].chars().next(),
                ))
            }
        })
}

fn unary(
    instr: &'static str,
    to_instr: fn(Reg) -> Instr,
) -> impl Parser<Token, Op, Error = SpannedErr> {
    ident()
        .try_map(move |(s, tok), _| {
            let reg = just_ignore_case(instr)
                .ignore_then(reg())
                .parse(s)
                .map_err(|_| {
                    let expected = TokenType::Ident(
                        format!("{}R", instr)
                    );
                    SpannedErr::expected_found(
                        tok.span.clone(), expected, tok.typ.clone()
                    )
                })?;

            Ok(Op::Instr {
                instr: to_instr(reg),
                op_span: tok.span,
                arg_span: None,
            })
        })
}

fn amode(mode: AddrMode)
    -> impl Parser<char, AddrMode, Error = Simple<char>>
{
    just_ignore_case(mode.spec()).to(mode)
}

fn addrmode(
) -> impl Parser<Token, (AddrMode, Token), Error = SpannedErr> {
    ident()
        .try_map(|(s, tok), _| {
            choice((
                amode(AddrMode::StackDeferredIndexed),
                amode(AddrMode::StackIndexed),
                amode(AddrMode::StackRelativeDeferred),
                amode(AddrMode::Immediate),
                amode(AddrMode::Direct),
                amode(AddrMode::Indirect),
                amode(AddrMode::StackRelative),
                amode(AddrMode::Indexed),
            ))
                .parse(s)
                .map(|mode| (mode, tok.clone()) )
                .map_err(|_| SpannedErr::expected_found(
                    tok.span,
                    TokenType::Ident("address mode".to_string()),
                    tok.typ,
                ))
        })
}

fn binary(instr: &'static str, to_op: fn(AddrMode, Value) -> Instr)
    -> impl Parser<Token, Op, Error = SpannedErr>
{
    ident()
        .try_map(move |(s, tok), _| {
            if s.eq_ignore_ascii_case(instr) {
                Ok(tok)
            } else {
                Err(SpannedErr::expected_found(
                    tok.span, TokenType::Ident(instr.to_string()), tok.typ
                ))
            }
        })
        .then_ignore(space())
        .then(value())
        .then(
            space().repeated()
                .ignore_then(comma())
                .ignore_then(space().repeated())
                .ignore_then(addrmode())
                .repeated()
                .at_most(1)
        )
        .map(move |((op_tok, (val, val_tok)), addrmode)| {
            let (addrmode, mode_span) = match &addrmode[..] {
                [] =>
                    (AddrMode::Immediate, val_tok.span.end..val_tok.span.end),
                [(amode, tok)] => (*amode, tok.span.clone()),
                _ => unreachable!(),
            };

            Op::Instr {
                instr: to_op(addrmode, val),
                op_span: op_tok.span,
                arg_span: Some((val_tok.span, mode_span)),
            }
        })
}

fn binary_reg(instr: &'static str, to_op: fn(Reg, AddrMode, Value) -> Instr)
    -> impl Parser<Token, Op, Error = SpannedErr>
{
    ident()
        .try_map(move |(s, tok), _| {
            let mnemonic_parser = just_ignore_case(instr)
                .ignore_then(reg());

            match mnemonic_parser.parse(s) {
                Ok(reg) => Ok((tok, reg)),
                Err(_) =>  Err(SpannedErr::expected_found(
                    tok.span, TokenType::Ident(format!("{}R", instr)), tok.typ
                )),
            }
        })
        .then_ignore(space())
        .then(value())
        .then_ignore(space().repeated())
        .then_ignore(comma())
        .then_ignore(space().repeated())
        .then(addrmode())
        .map(move |(((op_tok, reg), (val, val_tok)), (addrmode, mode_tok))| {
            Op::Instr {
                instr: to_op(reg, addrmode, val),
                op_span: op_tok.span,
                arg_span: Some((val_tok.span, mode_tok.span)),
            }
        })
}

fn binary_size(
    instr: &'static str,
    to_op: fn(Size, Reg, AddrMode, Value) -> Instr,
) -> impl Parser<Token, Op, Error = SpannedErr> {
    ident()
        .try_map(move |(s, tok), _| {
            let mnemonic_parser = just_ignore_case(instr)
                .ignore_then(choice((
                    choice((just('b'), just('B'))).to(Size::Byte),
                    choice((just('w'), just('W'))).to(Size::Word),
                )))
                .then(reg());

            match mnemonic_parser.parse(s) {
                Ok((size, reg)) => Ok((tok, size, reg)),
                Err(_) => {
                    let expected = format!("{}SR", instr);
                    Err(SpannedErr::expected_found(
                        tok.span, TokenType::Ident(expected), tok.typ
                    ))
                }
            }
        })
        .then_ignore(space())
        .then(value())
        .then_ignore(space().repeated())
        .then_ignore(comma())
        .then_ignore(space().repeated())
        .then(addrmode())
        .map(move |(((op_tok, size, reg), (val, val_tok)), (addrmode, mode_tok))|
            Op::Instr {
                instr: to_op(size, reg, addrmode, val),
                op_span: op_tok.span,
                arg_span: Some((val_tok.span, mode_tok.span)),
            }
        )
}

fn parser() -> impl Parser<Token, Vec<Line>, Error = SpannedErr> {
    let byte = choice((
        char_lit(),
        ascii()
            .try_map(|(s, tok), _| if let Some(c) = s.chars().next() {
                Ok((c as u8, tok))
            } else {
                Err(SpannedErr::new(tok.span, Error::AsciiByteTooLong))
            }),
        number()
            .try_map(|(n, tok), _| match u8::try_from(n) {
                Ok(byte) => Ok((byte, tok)),
                Err(_) => Err(SpannedErr::new(tok.span, Error::NumByteTooBig)),
            }),
    ));

    let dotop = choice((
        dotop(tokens::Dotop::Addrss, ident(), Dotop::Addrss),
        dotop(tokens::Dotop::Align, number(), Dotop::Align),
        dotop(tokens::Dotop::Ascii, ascii(), Dotop::Ascii),
        dotop(tokens::Dotop::Block, number(), Dotop::Block),
        dotop(tokens::Dotop::Burn, number(), Dotop::Burn), //TODO: ?
        dotop(tokens::Dotop::Byte, byte, Dotop::Byte),
        dotop(tokens::Dotop::Equate, literal(), Dotop::Equate),
        dotop(tokens::Dotop::Word, literal(), Dotop::Word),
    ))
        .map(|(op, op_tok, arg_tok)| Op::Dotop {
            op,
            op_span: op_tok.span,
            arg_span: Some(arg_tok.span),
        });

    let dotop = choice((
        dotop,
        filter_map(|_, tok: Token| match tok.typ {
            TokenType::Dotop(tokens::Dotop::End) => Ok(Op::Dotop {
                op: Dotop::End,
                op_span: tok.span,
                arg_span: None
            }),
            ty => Err(SpannedErr::expected_found(
                tok.span, TokenType::Dotop(tokens::Dotop::End), ty
            )),
        }),
    ));

    let label = filter_map(|_, tok: Token| match tok.typ {
        TokenType::LabelDef(sym) => Ok(sym),
        ty => Err(SpannedErr::expected_found(
            tok.span, TokenType::LabelDef("".to_string()), ty
        )),
    });

    let unary_basic = choice((
        caseless_keyword("STOP").map(|tok| (Instr::Stop, tok) ),
        caseless_keyword("RET").map(|tok| (Instr::Ret, tok) ),
        caseless_keyword("RETTR").map(|tok| (Instr::RetTr, tok) ),
        caseless_keyword("MOVSPA").map(|tok| (Instr::MovSPA, tok) ),
        caseless_keyword("MOVFLGA").map(|tok| (Instr::MovFlgA, tok) ),
        caseless_keyword("MOVAFLG").map(|tok| (Instr::MovAFlg, tok) ),
    ))
        .map(|(instr, tok)| Op::Instr {
            instr,
            op_span: tok.span,
            arg_span: None,
        });

    let unary_reg = choice((
        unary("NOT", Instr::Not),
        unary("NEG", Instr::Neg),
        unary("ASL", Instr::Asl),
        unary("ASR", Instr::Asr),
        unary("ROL", Instr::Rol),
        unary("ROR", Instr::Ror),
    ));

    let unary_nop = ident()
        .try_map(|(s, tok), _| {
            let mnemonic_parser = just_ignore_case("NOP")
                .ignore_then(choice((
                    just('0').to(TrapNum::Zero),
                    just('1').to(TrapNum::One),
                )));

            match mnemonic_parser.parse(s) {
                Ok(tr_num) => Ok(Op::Instr {
                    instr: Instr::NopU(tr_num),
                    op_span: tok.span,
                    arg_span: None,
                }),
                Err(_) => Err(SpannedErr::expected_found(
                    tok.span, TokenType::Ident("NOPN".to_string()), tok.typ
                )),
            }
        });

    let binary_op = choice((
        binary("BR", Instr::Br),
        binary("BRLT", Instr::BrLt),
        binary("BRLE", Instr::BrLe),
        binary("BREQ", Instr::BrEq),
        binary("BRNE", Instr::BrNe),
        binary("BRGE", Instr::BrGe),
        binary("BRGT", Instr::BrGt),
        binary("BRC", Instr::BrC),
        binary("BRV", Instr::BrV),
        binary("CALL", Instr::Call),

        binary("NOP", Instr::Nop),
        binary("DECI", Instr::Deci),
        binary("DECO", Instr::Deco),
        binary("HEXO", Instr::Hexo),
        binary("STRO", Instr::Stro),

        binary("ADDSP", Instr::AddSP),
        binary("SUBSP", Instr::SubSP),
    ));

    let binary_reg = choice((
        binary_reg("ADD", Instr::Add),
        binary_reg("SUB", Instr::Sub),
        binary_reg("AND", Instr::And),
        binary_reg("OR", Instr::Or),
    ));

    let binary_size = choice((
        binary_size("CP", Instr::Cp),
        binary_size("LD", Instr::Ld),
        binary_size("ST", Instr::St),
    ));

    let op = choice((
        unary_basic,
        unary_reg,
        unary_nop,
        binary_op,
        binary_reg,
        binary_size,
    ))
        .labelled("mnemonic");

    let line = label
        .then_ignore(whitespace())
        .repeated()
        .then(choice((
            dotop,
            op,
        )))
        .then_ignore(
            space()
                .repeated()
                .then(choice((
                    comment(),
                    newline(),
                    end(),
                )))
        )
        .map(|(labels, op)| Line {
            labels,
            op,
        });

    whitespace()
        .ignore_then(
            line.then_ignore(whitespace())
                .repeated()
                .at_least(1)
        )
}

pub fn ir(asm: &str) -> (Option<Vec<Line>>, Vec<SpannedErr>) {
    let (tokens, tok_errs) = tokens::tokens(asm);

    let tok_errs = tok_errs.into_iter().map(SpannedErr::from);

    if let Some(tokens) = tokens {
        let (lines, mut parse_errs) = parser().parse_recovery(tokens);
        parse_errs.extend(tok_errs);
        (lines, parse_errs)
    } else {
        (None, tok_errs.collect())
    }
}

pub fn asm(asm: &str) -> Result<String, Vec<SpannedErr>> {
    let (maybe_lines, errs) = ir(asm);

    let lines = if let Some(lines) = maybe_lines {
        lines
    } else {
        return Err(errs);
    };

    let mut sym_table = HashMap::new();
    let mut pos = 0;
    let mut found_end = false;
    let mut errs = vec![];

    // Nasty hashset, to keep track of which symbols to offset when we are
    // assembling with a .burn
    let mut equated_symbols = HashSet::new();
    let mut burn_at = None;

    for line in lines.iter() {
        for sym in line.labels.iter() {
            let val = match &line.op {
                Op::Dotop { op: Dotop::Equate(val), .. } => {
                    equated_symbols.insert(sym.as_str());
                    *val
                }
                _ => pos,
            };
            sym_table.insert(sym.as_str(), val);
        }

        match &line.op {
            Op::Instr {
                instr,
                arg_span: Some((_, mode_span)),
                ..
            } => {
                if let Err(err) = instr.check_addrmode() {
                    // Error::AddrMode
                    let e = SpannedErr { err, span: mode_span.clone() };
                    errs.push(e);
                }
            }
            Op::Dotop {
                op,
                arg_span: Some(arg_span),
                ..
            } => {
                if let Err(err) = op.check_arg() {
                    // Error::Align
                    let e = SpannedErr { err, span: arg_span.clone() };
                    errs.push(e);
                }
                if let Dotop::Burn(at) = op {
                    //TODO: Err on multiple burns
                    burn_at = Some(at);
                }
            }
            Op::Dotop { op: Dotop::End, .. } => {
                found_end = true;
                break;
            }
            _ => {}
        }

        pos += line.op.size(pos);
    }

    if let Some(burn_at) = burn_at {
        let start_addr = burn_at - pos + 1; // Why + 1?
        for (sym, val) in sym_table.iter_mut() {
            if ! equated_symbols.contains(sym) {
                *val += start_addr;
            }
        }
    } else {
        // The canonical implementation inserts these into the symbol table as
        // they are used. I don't like implicit things
        sym_table.insert("charIn", 0xfc15);
        sym_table.insert("charOut", 0xfc16);
    }

    /*
    for (key, val) in sym_table.iter() {
        eprintln!("\t\"{}\":{:>8x}", key, val);
    }*/

    let mut obj = String::with_capacity((pos * 3) as usize);
    let mut pos = 0;

    for line in lines.iter() {
        let objcode = match &line.op {
            Op::Dotop { op: Dotop::Burn(_), .. } => {
                // See Warford 471 for more on .Burn
                obj.clear();
                String::new()
            },
            Op::Dotop { op: Dotop::End, .. } => break,
            Op::Instr {
                arg_span: Some((val_span, _)),
                ..
            } => {
                match line.op.obj(pos, &sym_table) {
                    Ok(o) => o,
                    // Error::UnknownSymbol
                    Err(err) => {
                        let e = SpannedErr { err, span: val_span.clone() };
                        errs.push(e);
                        String::new()
                    }
                }
            }
            _ => {
                line.op.obj(pos, &sym_table)
                    .expect("obj can't fail here")
            }
        };

        if objcode.trim().len() != 0 {
            obj.push_str(&objcode);
            obj.push(' ');
        }

        pos += line.op.size(pos);
    }
    obj.push_str("zz");

    if !found_end {
        errs.push(SpannedErr::new(asm.len()..asm.len(), Error::MissingEnd));
    }

    if errs.len() != 0 {
        errs.sort_by(|e1, e2| e1.span.start.cmp(&e2.span.start) );
        Err(errs)
    } else {
        Ok(obj)
    }
}

