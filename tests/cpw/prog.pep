br main
yes:	.ascii "Yes\x00"
no:	.ascii "No\x00"

main:	ldwa 20,i
	cpwa 10,i
	brgt no_o
yes_o:	stro yes,d
	br end
no_o:	stro no,d
end:	stop
.end
