br main
arr:	.block 10

main:	ldwx 0,i
	
loop1:	cpwx 10,i
	brge loop2
	
	ldba charIn,d
	stba arr,x
	
	addx 1,i
	br loop1
	
loop2:	subx 1,i
	cpwx 0,i
	brlt end
	
	ldba arr,x
	stba charOut,d
	
	br loop2
	
end:	stop
	.end
