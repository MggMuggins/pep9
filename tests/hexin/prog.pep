	br main
hpPtr:	.addrss heap

MASK_lsb:	.equate 1
MASK_2ls:	.equate 0x0003
MASK_4ls:	.equate 0x000f
MASK_7ls:	.equate 0x007f
MASK_9ls:	.equate 0x01ff
MASK_10l:	.equate 0x03ff

; bufprint(*buf, len)
bp_buf:	.equate 2
bp_len:	.equate 4

bufprint:	ldwx 0,i
	
bp_for:	cpwx bp_len,s
	brge bp_end
	
	hexo bp_buf,sfx
	stro newline,d
	
	addx 2,i
	br bp_for
	
bp_end:	ret

; asbyte(byte) -> byte
ab_char:	.equate 4
ab_ret:	.equate 2
asbyte:	ldwa ab_char,s
	
	; Convert char to uppercase
	cpwa 'Z',i	; Lowercase is higher than uppercase
	brle ab_dcod
	suba 0x20,i
	
ab_dcod:	cpwa '0',i
	brlt ab_err
	cpwa '9',i
	brle ab_dig
	
	cpwa 'A',i
	brlt ab_err
	cpwa 'F',i
	brle ab_let
	br ab_err
	
ab_dig:	suba 0x30,i
	br ab_end
	
ab_let:	suba 0x37,i
	br ab_end
	
ab_err:	stro ab_ermsg,d
	hexo ab_char,s
	stop
	
ab_end:	stwa ab_ret,s
	ret

; hexin(*buf, len)
hi_buf:	.equate 4
hi_len:	.equate 6

hi_byte:	.equate 0

hexin:	subsp 2,i
	ldwx 0,i
	
hi_for:	cpwx hi_len,s
	brge hi_end
	
	ldwa 0,i
	ldba charIn,d	; Filter spaces/newlines
	cpwa ' ',i
	breq hi_for
	cpwa '\n',i
	breq hi_for
	
	subsp 4,i
	stwa 2,s
	call asbyte
	ldwa 0,i
	addsp 4,i
	
	movaflg	; clear carry bit
	ldwa -4,s	; Grab the return value of as_byte
	rola
	rola
	rola
	rola
	stwa hi_byte,s
	
hi_two:	ldwa 0,i
	ldba charIn,d	; Filter spaces/newlines
	cpwa ' ',i
	breq hi_two
	cpwa '\n',i
	breq hi_two
	
	subsp 4,i
	stwa 2,s
	call asbyte
	ldwa 0,s
	addsp 4,i
	
	ora hi_byte,s
	stba hi_buf,sfx
	
	addx 1,i
	br hi_for
	
hi_end:	addsp 2,i
	ret

; malloc()
; Precondition: A contains a number of bytes
; Postcondition: X contains a pointer to bytes
malloc:	ldwx hpPtr,d
	adda hpPtr,d
	stwa hpPtr,d
	ret

; main()
buf:	.equate 0

LEN:	.equate 4

main:	subsp 2,i
	
	ldwa LEN,i
	call malloc
	stwx buf,s
	
	ldwa LEN,i
	stwa -2,s
	
	ldwa buf,s
	stwa -4,s
	
	subsp 4,i
	call hexin
	call bufprint
	addsp 4,i
	
	addsp 2,i
	stop

ab_ermsg:	.ascii "Invalid hex digit: \x00"
parmsep:	.ascii ", \x00"
newline:	.ascii "\n\x00"
heap:	.byte 0
.end

