use pep9::asm;

const OS_ASM: &str = include_str!("os.pep");
const OS_OBJ: &str = include_str!("os.pepo");

#[test]
fn asm_os() {
    let our_obj = asm::asm(OS_ASM).unwrap();
    
    asm::obj_eq(OS_OBJ, &our_obj);
}

