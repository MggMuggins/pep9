; Co-written with Bach Phan

BR main
prompt:.ASCII "Enter a number\x00"
result:.ASCII "Fibonacci number is\x00"
endl:.ASCII "\n\x00"

retval:.EQUATE 4 ;return value #2d
n:.EQUATE 2 ;formal parameter #2d
fib: LDWA n, s 
     CPWA 3, i
     BRGE else
     STWA retval, s
     RET
else: LDWA n, s
      SUBA 1, i
      STWA -4, s
      SUBSP 4, i ;push #retval #n 
      CALL fib
      ADDSP 4, i ;pop #n #retval
      LDWA -2, s
      STWA retval, s
      LDWA n, s
      SUBA 2, i
      STWA -4, s
      SUBSP 4, i ;push #retval #n
      CALL fib
      ADDSP 4, i ;pop #n #retval
      LDWA -2, s
      ADDA retval, s
      STWA retval, s
      RET

x:.EQUATE 2 ;local variable #2d
z:.EQUATE 0 ;local variable #2d
main: SUBSP 4, i ;push #x #z 
      STRO prompt, d
      DECI x, s
      LDWA x, s
      STWA -4, s
      SUBSP 4, i ;push #retval #n
      CALL fib
      ADDSP 4, i ;pop #n #retval
      LDWA -2, s
      STWA z, s
      DECO x, s
      STRO result, d
      DECO z, s
      STRO endl, d
      ADDSP 4, i ;pop #z #x 
      STOP
      .END
      