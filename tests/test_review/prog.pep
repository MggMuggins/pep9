br main
prompt:  .ascii "Enter a number\x00"
ans:     .ascii "The smallest is \x00"
endl:    .ascii "\n\x00"

; getNumber(x*)
gn_x:    .equate 2
getNum:  stro prompt,d
         deci gn_x,sf
         ret

; smallest(x, y) -> smallest
s_x:     .equate 4
s_y:     .equate 2
smallest: ldwa s_x,s
         cpwa s_y,s
         brge else
         stwa 6,s
         br end
else:    ldwa s_y,s
         stwa 6,s
end:     ret

; main()
a:       .equate 4
b:       .equate 2
c:       .equate 0
main:    subsp 6,i
         
         movspa
         adda a,i
         stwa -2,s 
         subsp 2,i
         call getNum
         addsp 2,i
         
         movspa
         adda b,i
         stwa -2,s
         subsp 2,i
         call getNum
         addsp 2,i
         
         ldwa a,s
         stwa -4,s
         ldwa b,s
         stwa -6,s
         subsp 6,i
         call smallest
         addsp 6,i
         ldwa -2,s
         stwa c,s
         
         stro ans,d 
         deco c,s
         stro endl,d 
         addsp 6,i
         stop
.end