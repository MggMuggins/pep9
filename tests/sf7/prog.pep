; Welcome to the Pep9 Web IDE!
; You can get started with some Pep9 code in the editor.
; Use the terminal below to assemble the current editor

; void main() {
;	int n;
;	int* x = &n;
;	
;	*x = 7;
;
;	cout << n << endl;
; }

br main
endl:	.equate '\n'

x:	.equate 0
n:	.equate 2

main:	subsp 2,i	; push n#2d
	
	movspa
	subsp 2,i	; push x#2d ;WARNING: given trace tag, but no symbol
	stwa x,s
	
	ldwa 7,i
	stwa x,sf
	
	deco n,s
	ldba endl,i
	stba 0xfc16,d
	stop
	.end