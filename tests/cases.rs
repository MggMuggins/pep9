use std::fs;
use std::path::{Path, PathBuf};

use ariadne::Source;
use pep9::asm;
use pep9::cpu::{Cpu, CpuStatus};
use test_generator::test_resources;

fn canon_obj(src_path: &str) -> Option<String> {
    let mut obj_path = PathBuf::from(src_path);
    obj_path.set_file_name("prog.pepo");

    fs::read_to_string(&obj_path).ok()
}

fn canon_output(src_path: &str) -> Option<String> {
    let mut out_path = PathBuf::from(src_path);
    out_path.set_file_name("out.txt");

    fs::read_to_string(&out_path).ok()
}

fn input_of(src_path: &str) -> Option<String> {
    let mut in_path = PathBuf::from(src_path);
    in_path.set_file_name("in.txt");

    fs::read_to_string(&in_path).ok()
}

#[test_resources("tests/*/prog.pep")]
fn run(src_path: &str) {
    let src_path = Path::new(src_path)
        .to_str()
        .expect("path wasn't unicode");

    let asm = fs::read_to_string(src_path)
        .expect("failed to read src");

    let obj = match asm::asm(&asm) {
        Ok(obj) => obj,
        Err(errs) => {
            for err in errs {
                err.report(src_path)
                    .eprint((src_path, Source::from(&asm)))
                    .expect("failed to print report");
            }
            panic!("asm encountered errors");
        }
    };

    eprintln!("Obj: `{}`", obj);
    let canon_obj = canon_obj(src_path)
        .expect("object code required");
    asm::obj_eq(&canon_obj, &obj);

    let mut cpu = Cpu::new();
    cpu.input(obj.as_bytes());

    let load_status = cpu.load();
    assert_eq!(CpuStatus::Stopped, load_status);
    eprintln!("Load succeeded");

    if let Some(input) = input_of(src_path) {
        eprintln!("Input: `{}`", input.escape_debug());
        cpu.input(input.as_bytes());
    }

    let (status, output) = cpu.run();

    if let Some(canon_output) = canon_output(src_path) {
        let display_out = output.iter().copied()
            .map(|b| char::try_from(b)
                .map(|c| c.escape_debug().to_string() )
                .unwrap_or("\\{invalid unicode}".to_string() )
            )
            .collect::<String>();
        eprintln!("Expected: `{}`", canon_output.escape_debug());
        eprintln!("Got:      `{}`", display_out);
        assert_eq!(canon_output.as_bytes(), output);
    } else {
        assert_eq!(Vec::<u8>::new(), output);
    }

    assert_eq!(CpuStatus::Stopped, status);
}

