         subsp 2,i
         ldwa arr,i
         stwa 0,s      ; push pointer to arr
         
         ldwx 0,i
loop:    cpwx arr_len,i
         breq end
         
         hexo 0,sfx
         addx 2,i
         
         br loop
end:     stop

arr_len: .equate 10
arr:     .word 0xcafe
         .word 0xf00d
         .byte 0x13
         .word 0xdead
         .word 0xbeef
         .byte 0
.end