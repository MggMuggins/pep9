; Welcome to the Pep9 Web IDE!
; You can get started with some Pep9 code in the editor.
; Use the terminal below to assemble the current editor

; void main() {
;	int n;
;	int* x = &n;
;	
;	*x = 7;
;
;	cout << n << endl;
; }
	br main
arr:	.block 10
len:	.equate 10

main:	ldwx 0,i
loop:	cpwx len,i
	brge loop2
	
	deci arr,x
	
	addx 2,i
	br loop
	
loop2:	subx 2,i
	cpwx 0,i
	brlt end
	
	deco arr,x
	ldba ' ',i
	stba charOut,d
	
	br loop2
	
end:	stop
.end